package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.Optional;
import java.math.BigDecimal;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.internna.ossmoney.AbstractSecuredTest;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.model.budget.Budget;
import org.internna.ossmoney.model.budget.BudgetDTO;
import org.internna.ossmoney.model.transaction.Tags;
import org.internna.ossmoney.services.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class BudgetServiceImplTest extends AbstractSecuredTest {

    private @Autowired BudgetService budgetService;

    @Test(expected = IllegalArgumentException.class)
    public void testSave() {
        final BudgetDTO dto = new BudgetDTO();
        assertThat(budgetService.save(null).isPresent(), is(false));
        dto.setName("budget");
        dto.setFrom(new Date());
        dto.setSubcategory(Tags.CAR_BUY);
        dto.setAmount(new BigDecimal("200"));
        Optional<Budget> budget = budgetService.save(dto);
        assertThat(budget.isPresent(), is(true));
        assertThat(budget.get().getTo(), is(notNullValue()));
        assertThat(budget.get().getName(), is(equalTo("budget")));
        assertThat(budget.get().getSubcategory(), is(equalTo(Tags.CAR_BUY)));
        assertThat(budget.get().getFrom().compareTo(budget.get().getTo()), is(equalTo(-1)));
        assertThat(budget.get().getAmount().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("200"))));
        dto.setName("budget2");
        dto.setUuid(budget.get().getUuid());
        dto.setAmount(new BigDecimal("300"));
        final Date to = budget.get().getTo();
        dto.setTo(new Date(to.getTime() + 26236423));
        budget = budgetService.save(dto);
        assertThat(budget.isPresent(), is(true));
        assertThat(budget.get().getName(), is(equalTo("budget2")));
        assertThat(budget.get().getTo().compareTo(to), is(equalTo(1)));
        assertThat(budget.get().getAmount().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("300"))));
        loginAsTestUser();
        budgetService.save(dto);
    }

    @Test
    public void testRetrieveBudget() {
        final Collection<Budget> budgets = budgetService.retrieveBudget();
        assertThat(budgets, hasSize(3));
        final Budget budget = budgets.iterator().next();
        assertThat(budget.getTransactions(), anyOf(hasSize(2), hasSize(1)));
        assertThat(budget.getSpent(), anyOf(equalTo(new BigDecimal("250")), equalTo(new BigDecimal("60")), equalTo(BigDecimal.ZERO)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemove() {
        assertThat(budgetService.remove(null), is(false));
        assertThat(budgetService.remove("1234"), is(false));
        assertThat(budgetService.remove("1"), is(true));
        loginAsTestUser();
        budgetService.remove("2");
    }

}
