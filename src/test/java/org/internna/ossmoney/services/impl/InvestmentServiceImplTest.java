package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.Optional;
import java.math.BigDecimal;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.internna.ossmoney.AbstractSecuredTest;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.pm.PreciousMetal;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct;
import org.internna.ossmoney.model.investment.pm.PreciousMetalTransactionDTO;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct.PreciousMetalMint;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct.PreciousMetalShape;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class InvestmentServiceImplTest extends AbstractSecuredTest {

    private @Autowired AccountService accountService;
    private @Autowired InvestmentService investmentService;

    @Test
    public void testRetrievePortfolio() {
        final Optional<InvestmentPortfolio> portfolio = investmentService.retrievePortfolio();
        assertThat(portfolio.isPresent(), is(true));
        assertThat(portfolio.get().getDeposits(), hasSize(2));
        assertThat(portfolio.get().getPreciousMetals(), hasSize(1));
        assertThat(portfolio.get().getDeposits().iterator().next().getTransactions(), hasSize(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindDeposit() {
        Optional<Deposit> deposit = investmentService.findDeposit(null);
        assertThat(deposit.isPresent(), is(false));
        deposit = investmentService.findDeposit("23423");
        assertThat(deposit.isPresent(), is(false));
        deposit = investmentService.findDeposit("2");
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getTransactions(), hasSize(2));
        loginAsTestUser();
        investmentService.findDeposit("1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCancel() {
        final DepositDTO dto = new DepositDTO();
        Optional<Deposit> deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(false));
        dto.setUuid("3242");
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(false));
        dto.setUuid("2");
        dto.setOpened(new Date());
        dto.setBalance(new BigDecimal("-100"));
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getTransactions(), hasSize(3));
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("400"))));
        dto.setBalance(new BigDecimal("-1000"));
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(false));
        deposit = investmentService.findDeposit("2");
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(BigDecimal.ZERO)));
        dto.setUuid("1");
        dto.setAccount("1");
        dto.setOpened(new Date());
        dto.setInterestAccount("1");
        dto.setInterest(BigDecimal.TEN);
        dto.setBalance(new BigDecimal("-100"));
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getTransactions(), hasSize(3));
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("200"))));
        assertThat(deposit.get().getInterest().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("10"))));
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getTransactions(), hasSize(4));
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("100"))));
        assertThat(deposit.get().getInterest().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("20"))));
        final Optional<Account> account = accountService.find("1");
        assertThat(account.isPresent(), is(true));
        assertThat(account.get().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("11284.50"))));
        loginAsTestUser();
        investmentService.cancel(dto);
    }

    @Test
    public void testCreateDeposit() {
        DepositDTO dto = null;
        assertThat(investmentService.save(dto).isPresent(), is(false));
        dto = new DepositDTO();
        dto.setDuration(3);
        dto.setName("deposit");
        dto.setOpened(new Date());
        dto.setBalance(BigDecimal.TEN);
        dto.setFinancialInstitution("1");
        dto.setAnnualPercentageYield(1.3F);
        final Optional<Deposit> deposit = investmentService.save(dto);
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getDuration(), is(equalTo(3)));
        assertThat(deposit.get().getTransactions(), hasSize(1));
        assertThat(deposit.get().getName(), is(equalTo("deposit")));
        assertThat(deposit.get().getAnnualPercentageYield(), is(equalTo(1.3F)));
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(BigDecimal.TEN)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateDeposit() {
        final DepositDTO dto = new DepositDTO();
        dto.setUuid("1");
        dto.setDuration(3);
        dto.setName("deposit");
        dto.setOpened(new Date());
        dto.setBalance(BigDecimal.TEN);
        dto.setFinancialInstitution("1");
        dto.setAnnualPercentageYield(1.3F);
        final Optional<Deposit> deposit = investmentService.save(dto);
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getUuid(), is(equalTo("1")));
        assertThat(deposit.get().getDuration(), is(equalTo(3)));
        assertThat(deposit.get().getTransactions(), hasSize(2));
        assertThat(deposit.get().getName(), is(equalTo("deposit")));
        assertThat(deposit.get().getAnnualPercentageYield(), is(equalTo(1.3F)));
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(BigDecimal.TEN)));
        assertThat(deposit.get().getTransactions().get(0).getAmount().getNumber().numberValue(BigDecimal.class), is(equalTo(BigDecimal.TEN)));
        loginAsTestUser();
        investmentService.save(dto);
    }

    @Test
    public void testCreateMetalProduct() {
        assertThat(investmentService.save((PreciousMetalProduct) null).isPresent(), is(false));
        final PreciousMetalProduct metal = new PreciousMetalProduct();
        metal.setName("product");
        metal.setShape(PreciousMetalShape.COIN);
        metal.setMint(PreciousMetalMint.CHINA_MINT);
        metal.setMetalType(PreciousMetalType.PALLADIUM);
        final Optional<PreciousMetalProduct> product = investmentService.save(metal);
        assertThat(product.isPresent(), is(true));
        assertThat(product.get().getUuid(), is(notNullValue()));
        assertThat(product.get().getName(), is(equalTo("product")));
        assertThat(product.get().getShape(), is(equalTo(PreciousMetalShape.COIN)));
        assertThat(product.get().getMetalType(), is(equalTo(PreciousMetalType.PALLADIUM)));
    }

    @Test
    public void testObtainPreciousMetalProducts() {
        final Collection<PreciousMetalProduct> products = investmentService.obtainPreciousMetalProducts();
        assertThat(products, is(notNullValue()));
        assertThat(products, hasSize(14));
    }

    @Test
    public void testBuyPreciousMetal() {
        final PreciousMetal originalPreciousMetal = investmentService.retrievePortfolio().get().getPreciousMetals().iterator().next();
        final int transactions = originalPreciousMetal.getTransactions().size();
        Optional<PreciousMetal> preciousMetal = investmentService.buyPreciousMetal(null);
        assertThat(preciousMetal, is(notNullValue()));
        assertThat(preciousMetal.isPresent(), is(false));
        final PreciousMetalTransactionDTO dto = new PreciousMetalTransactionDTO();
        preciousMetal = investmentService.buyPreciousMetal(dto);
        assertThat(preciousMetal.isPresent(), is(false));
        configure(dto);
        preciousMetal = investmentService.buyPreciousMetal(dto);
        assertThat(preciousMetal.isPresent(), is(true));
        assertThat(preciousMetal.get().getWeight(), is(equalTo(48.425F)));
        assertThat(preciousMetal.get().getTransactions(), hasSize(transactions + 1));
    }

    @Test
    public void testBuyPreciousMetal_withTransaction() {
        final PreciousMetalTransactionDTO dto = new PreciousMetalTransactionDTO();
        configure(dto);
        dto.setAccount("1");
        dto.setProduct("14");
        dto.setMetalType(PreciousMetalType.SILVER);
        Optional<PreciousMetal> preciousMetal = investmentService.buyPreciousMetal(dto);
        final Optional<Account> account = accountService.find("1");
        assertThat(account.isPresent(), is(true));
        assertThat(preciousMetal.get().getTransactions(), hasSize(1));
        assertThat(preciousMetal.get().getWeight(), is(equalTo(10F)));
        assertThat(account.get().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("11054.50"))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuyPreciousMetal_withInvalidAccount() {
        final PreciousMetalTransactionDTO dto = new PreciousMetalTransactionDTO();
        configure(dto);
        dto.setAccount("1");
        loginAsTestUser();
        investmentService.buyPreciousMetal(dto);
    }

    private void configure(final PreciousMetalTransactionDTO dto) {
        dto.setProduct("1");
        dto.setQuantity(1);
        dto.setCurrency("EUR");
        dto.setWeight(BigDecimal.TEN);
        dto.setAmount(BigDecimal.TEN);
        dto.setOperationDate(new Date());
        dto.setWeightInMetal(BigDecimal.TEN);
        dto.setMetalType(PreciousMetalType.GOLD);
    }

}
