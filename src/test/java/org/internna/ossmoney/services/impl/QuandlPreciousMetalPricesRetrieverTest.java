package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;
import org.internna.ossmoney.services.PreciousMetalPricesRetriever;
import org.internna.ossmoney.model.investment.pm.PreciousMetalPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class QuandlPreciousMetalPricesRetrieverTest {

    private @Autowired PreciousMetalPricesRetriever retriever;

    @Test
    public void testRetrievePreciousMetalPrice() {
        final Optional<PreciousMetalPrice> price = retriever.retrievePreciousMetalPrice(PreciousMetalType.GOLD);
        assertThat(price.isPresent(), is(true));
        assertThat(price.get().getDate().after(DateUtils.add(new Date(), -10)), is(true));
    }

}
