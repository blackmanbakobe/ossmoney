package org.internna.ossmoney.services.impl;

import java.util.Locale;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.internna.ossmoney.AbstractSecuredTest;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.security.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class UserServiceImplTest extends AbstractSecuredTest {

    private @Autowired UserService userService;

    @Test
    public void testCurrentUser() {
        assertThat(userService.currentUser().isPresent(), is(true));
        assertThat(userService.currentUser().get().getUsername(), is(equalTo("demo")));
        loginAsTestUser();
        assertThat(userService.currentUser(), is(notNullValue()));
        assertThat(userService.currentUser().get().getUsername(), is(equalTo("test")));
    }

    @Test
    public void testUpdate() {
        UserDTO dto = null;
        assertThat(userService.update(dto).isPresent(), is(false));
        dto = new UserDTO();
        dto.setSkin("skin");
        dto.setLocale("en_US");
        dto.setCurrency("USD");
        final Optional<User> updated = userService.update(dto);
        assertThat(updated.isPresent(), is(true));
        assertThat(updated.get().getSkin(), is(equalTo("skin")));
        assertThat(updated.get().getLocale(), is(equalTo(Locale.US)));
        assertThat(updated.get().getCurrency().getCurrencyCode(), is(equalTo("USD")));
    }

}