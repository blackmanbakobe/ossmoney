package org.internna.ossmoney.model.investment;

import org.hibernate.annotations.Filter;
import java.util.Set;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.AbstractEntity;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.pm.PreciousMetal;

@Entity
@Table(name = InvestmentPortfolio.PORTFOLIO)
public class InvestmentPortfolio extends AbstractEntity {

    public static final String PORTFOLIO = "INVESTMENT_PORTFOLIO";

    public InvestmentPortfolio() {
        // Default JPA/JSON constructor
    }

    public InvestmentPortfolio(final User owner) {
        this.owner = owner;
    }

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "OWNER", nullable = false, updatable = false)
    private User owner;

    @Filter(name = AbstractEntity.VISIBLE_ENTITY, condition = "visible = :" + AbstractEntity.VISIBLE_PARAM)
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, mappedBy = "portfolio")
    private Set<Deposit> deposits;

    @Filter(name = AbstractEntity.VISIBLE_ENTITY, condition = "visible = :" + AbstractEntity.VISIBLE_PARAM)
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, mappedBy = "portfolio")
    private Set<PreciousMetal> preciousMetals;

    public final User getOwner() {
        return owner;
    }

    public final void setOwner(final User owner) {
        this.owner = owner;
    }

    public final Set<Deposit> getDeposits() {
        return deposits;
    }

    public final void setDeposits(final Set<Deposit> deposits) {
        this.deposits = deposits;
    }

    public final Set<PreciousMetal> getPreciousMetals() {
        return preciousMetals;
    }

    public final void setPreciousMetals(final Set<PreciousMetal> preciousMetals) {
        this.preciousMetals = preciousMetals;
    }

}
