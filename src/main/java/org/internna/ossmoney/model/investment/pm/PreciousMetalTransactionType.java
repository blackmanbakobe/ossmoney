package org.internna.ossmoney.model.investment.pm;

public enum PreciousMetalTransactionType {

	BUY, SELL;

}
