package org.internna.ossmoney.model.investment.pm;

import java.util.List;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FetchMode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.internna.ossmoney.util.CollectionUtils;
import org.internna.ossmoney.model.AbstractEntity;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;

@Entity
@Table(name = PreciousMetal.PRECIOUS_METAL)
public class PreciousMetal extends AbstractEntity {

    public static final String PRECIOUS_METAL = "PRECIOUS_METAL";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "METAL", nullable = false)
    private PreciousMetalType preciousMetalType;

    @NotNull
    @Column(name = "WEIGHT", nullable = false, columnDefinition = "DECIMAL")
    private float weight;

    @Fetch(FetchMode.SELECT)
    @OrderBy("operationDate DESC")
    @Filter(name = AbstractEntity.VISIBLE_ENTITY, condition = "visible = :" + AbstractEntity.VISIBLE_PARAM)
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, mappedBy = "preciousMetal")
    private List<PreciousMetalTransaction> transactions;

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "PORTFOLIO", nullable = false, updatable = false)
    private InvestmentPortfolio portfolio;

    public final void addTransaction(final PreciousMetalTransaction transaction) {
        transactions = CollectionUtils.add(transactions, transaction);
    }

    public final PreciousMetalType getPreciousMetalType() {
        return preciousMetalType;
    }

    public final void setPreciousMetalType(final PreciousMetalType preciousMetalType) {
        this.preciousMetalType = preciousMetalType;
    }

    public final float getWeight() {
        return weight;
    }

    public final void setWeight(final float weight) {
        this.weight = weight;
    }

    public final List<PreciousMetalTransaction> getTransactions() {
        return transactions;
    }

    public final void setTransactions(final List<PreciousMetalTransaction> transactions) {
        this.transactions = transactions;
    }

    public final InvestmentPortfolio getPortfolio() {
        return portfolio;
    }

    public final void setPortfolio(final InvestmentPortfolio portfolio) {
        this.portfolio = portfolio;
    }

}
