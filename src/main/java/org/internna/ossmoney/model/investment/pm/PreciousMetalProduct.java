package org.internna.ossmoney.model.investment.pm;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import org.internna.ossmoney.model.AbstractEntity;

@Entity
@Table(name = PreciousMetalProduct.PRECIOUS_METAL_PRODUCT)
public class PreciousMetalProduct extends AbstractEntity {

    public static final String PRECIOUS_METAL_PRODUCT = "PRECIOUS_METAL_PRODUCT";

    public enum PreciousMetalShape {
        COIN, BAR, NUGGET, MEDAL, JEWEL;
    }

    public enum PreciousMetalMint {
        AUSTRIAN_MINT, CHINA_MINT, MEXICO, PERTH_MINT, ROYAL_CANADIAN_MINT, SOMALIA, SOUTH_AFRICA_MINT, UK_ROYAL_MINT, US_MINT, UNKNOWN_MINT;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "SHAPE", nullable = false)
    private PreciousMetalShape shape;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "METAL", nullable = false)
    private PreciousMetalType metalType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "MINT", nullable = false)
    private PreciousMetalMint mint;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "URL")
    private String url;

    public final PreciousMetalShape getShape() {
        return shape;
    }

    public final void setShape(final PreciousMetalShape shape) {
        this.shape = shape;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final PreciousMetalMint getMint() {
        return mint;
    }

    public final void setMint(final PreciousMetalMint mint) {
        this.mint = mint;
    }

    public final String getUrl() {
        return url;
    }

    public final void setUrl(final String url) {
        this.url = url;
    }

    public final PreciousMetalType getMetalType() {
        return metalType;
    }

    public final void setMetalType(final PreciousMetalType metalType) {
        this.metalType = metalType;
    }

}
