package org.internna.ossmoney.model.account;

import org.javamoney.moneta.Money;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Columns;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.util.Collections;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.persistence.EnumType;
import javax.persistence.Temporal;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.TemporalType;
import javax.money.MonetaryCurrencies;
import javax.validation.constraints.NotNull;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.util.MoneyUtils;
import org.internna.ossmoney.util.StringUtils;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.util.CollectionUtils;
import org.internna.ossmoney.model.AbstractEntity;
import org.internna.ossmoney.model.transaction.Transaction;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency;

@Entity
@Table(name = Account.ACCOUNT)
@TypeDef(name = Account.MONEY_TYPE, typeClass = PersistentMoneyAmountAndCurrency.class)
public class Account extends AbstractEntity {

    static final String MONEY_TYPE = "MONEY_TYPE";

    public static final String ACCOUNT = "ACCOUNT";
    public static final CurrencyUnit DEFAULT_CURRENCY = MonetaryCurrencies.getCurrency("EUR");

    public Account() {
        // Default JPA/JSON constructor
    }

    public Account(final String uuid) {
        this.setUuid(uuid);
    }

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull
    @Column(name = "FAVORITE", nullable = false)
    private Boolean favorite;

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "S-")
    @Column(name = "OPENED", nullable = false)
    private Date opened;

    @Column(name = "CLOSED")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "S-")
    private Date closed;

    @NotNull
    @Type(type = Account.MONEY_TYPE)
    @Columns(columns = {@Column(name = "INITIAL_BALANCE_CURRENCY", nullable = false), @Column(name = "INITIAL_BALANCE_AMOUNT", nullable = false)})
    private MonetaryAmount initialBalance;

    @NotNull
    @Type(type = Account.MONEY_TYPE)
    @Columns(columns = {@Column(name = "CURRENT_BALANCE_CURRENCY", nullable = false), @Column(name = "CURRENT_BALANCE_AMOUNT", nullable = false)})
    private MonetaryAmount currentBalance;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ACCOUNT_TYPE", nullable = false)
    private AccountType accountType;

    @NotNull
    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FINANCIAL_INSTITUTION", nullable = false)
    private FinancialInstitution financialInstitution;

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "OWNER", nullable = false)
    private User owner;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
    @Filter(name = AbstractEntity.VISIBLE_ENTITY, condition = "visible = :" + AbstractEntity.VISIBLE_PARAM)
    private List<Transaction> transactions;

    @PrePersist
    protected final void onAccountCreate() {
        if (getFavorite() == null) {
            setFavorite(Boolean.FALSE);
        }
        if (getOpened() == null) {
            setOpened(new Date());
        }
        if (getInitialBalance() == null) {
            setInitialBalance(Money.of(getFinancialInstitution().getCountry().getCurrency(), BigDecimal.ZERO));
        }
        if (getCurrentBalance() == null) {
            setCurrentBalance(getInitialBalance());
        }
    }

    public final void addTransaction(final Transaction transaction) {
        if (transaction != null) {
            transaction.setAccount(this);
            transactions.add(transaction);
        }
    }

    public final void addBalance(final MonetaryAmount amount) {
        if (amount != null) {
            setCurrentBalance(getCurrentBalance().add(amount));
        }
    }

    public final String asQIF() {
        final StringBuilder qif = new StringBuilder("!Account\n").append("N").append(getName()).append("\n").append("T").append(getAccountType().name()).append("\n");
        if (StringUtils.hasText(getDescription())) {
            qif.append("D\"").append(getDescription()).append("\"\n");
        }
        if (isCreditCard()) {
            qif.append("L").append(MoneyUtils.format(initialBalance)).append("\n^\n");
        } else {
            qif.append("^\n!Type:Bank\n").append(DateUtils.asQIF(opened)).append("\n").append("T").append(MoneyUtils.format(initialBalance)).append("\n").append("M###Opening Balance###\n^\n");
        }
        return qif.toString();
    }

    public final boolean isCreditCard() {
        return accountType != null && AccountType.CREDIT_CARD.equals(accountType);
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getIban() {
        return iban;
    }

    public final void setIban(final String iban) {
        this.iban = iban;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }

    public final Boolean getFavorite() {
        return favorite;
    }

    public final void setFavorite(final Boolean favorite) {
        this.favorite = favorite;
    }

    public final Date getOpened() {
        return opened;
    }

    public final void setOpened(final Date opened) {
        this.opened = opened;
    }

    public final MonetaryAmount getInitialBalance() {
        return initialBalance;
    }

    public final void setInitialBalance(final MonetaryAmount initialBalance) {
        this.initialBalance = initialBalance;
    }

    public final MonetaryAmount getCurrentBalance() {
        return currentBalance;
    }

    public final void setCurrentBalance(final MonetaryAmount currentBalance) {
        this.currentBalance = currentBalance;
    }

    public final User getOwner() {
        return owner;
    }

    public final void setOwner(final User owner) {
        this.owner = owner;
    }

    public final AccountType getAccountType() {
        return accountType;
    }

    public final void setAccountType(final AccountType accountType) {
        this.accountType = accountType;
    }

    public final FinancialInstitution getFinancialInstitution() {
        return financialInstitution;
    }

    public final void setFinancialInstitution(final FinancialInstitution financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public final List<Transaction> getTransactions() {
        return CollectionUtils.isEmpty(transactions) ? Collections.emptyList() : Collections.unmodifiableList(transactions);
    }

    public final void setTransactions(final List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public final Date getClosed() {
        return closed;
    }

    public final void setClosed(final Date closed) {
        this.closed = closed;
    }

}
