package org.internna.ossmoney.model.account;

import java.util.Date;
import java.math.BigDecimal;

import static org.springframework.util.StringUtils.hasText;

public final class AccountDTO {

    private Date opened;
    private boolean favorite;
    private AccountType accountType;
    private BigDecimal initialBalance;
    private String uuid, name, iban, description, financialInstitution;

    public boolean isFavorite() {
        return favorite;
    }

    public Date getOpened() {
        return opened;
    }

    public void setOpened(final Date opened) {
        this.opened = opened;
    }

    public void setFavorite(final boolean favorite) {
        this.favorite = favorite;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(final AccountType accountType) {
        this.accountType = accountType;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(final BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(final String iban) {
        this.iban = iban;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getFinancialInstitution() {
        return financialInstitution;
    }

    public void setFinancialInstitution(final String financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public Account getAccount() {
        final Account account = new Account();
        account.setOpened(opened);
        account.setName(this.name);
        account.setIban(this.iban);
        account.setFavorite(favorite);
        account.setAccountType(this.accountType);
        account.setDescription(this.description);
        account.setUuid(hasText(this.uuid) ? this.uuid : null);
        account.setFinancialInstitution(new FinancialInstitution(this.financialInstitution));
        return account;
    }

}
