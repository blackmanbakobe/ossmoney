package org.internna.ossmoney.model.security;

public final class UserDTO {

    private String skin, locale, currency;

    public String getSkin() {
        return skin;
    }

    public void setSkin(final String skin) {
        this.skin = skin;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

}
