package org.internna.ossmoney.services;

import java.util.Optional;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;
import org.internna.ossmoney.model.investment.pm.PreciousMetalPrice;

public interface PreciousMetalPricesRetriever {

    Optional<PreciousMetalPrice> retrieveGoldPrice();

    Optional<PreciousMetalPrice> retrieveSilverPrice();

    Optional<PreciousMetalPrice> retrievePlatinumPrice();

    Optional<PreciousMetalPrice> retrievePalladiumPrice();

    Optional<PreciousMetalPrice> retrievePreciousMetalPrice(PreciousMetalType preciousMetalType);

}
