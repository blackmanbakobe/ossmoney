package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.math.BigDecimal;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.util.CollectionUtils;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.data.AccountRepository;
import org.internna.ossmoney.data.DepositRepository;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.services.TransactionService;
import org.internna.ossmoney.data.PreciousMetalRepository;
import org.internna.ossmoney.model.transaction.TransactionDTO;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.pm.PreciousMetal;
import org.internna.ossmoney.model.transaction.TransactionType;
import org.internna.ossmoney.data.PreciousMetalPriceRepository;
import org.internna.ossmoney.data.InvestmentPortfolioRepository;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.data.FinancialInstitutionRepository;
import org.internna.ossmoney.data.PreciousMetalProductRepository;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.internna.ossmoney.services.PreciousMetalPricesRetriever;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;
import org.internna.ossmoney.model.investment.pm.PreciousMetalPrice;
import org.internna.ossmoney.data.PreciousMetalTransactionRepository;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct;
import org.internna.ossmoney.model.investment.deposit.DepositTransaction;
import org.internna.ossmoney.model.investment.pm.PreciousMetalTransaction;
import org.internna.ossmoney.model.investment.pm.PreciousMetalTransactionDTO;
import org.internna.ossmoney.model.investment.deposit.DepositTransactionType;

import static org.internna.ossmoney.util.StringUtils.hasText;

@Service
@Transactional
public final class InvestmentServiceImpl implements InvestmentService {

    private static final String INVALID_DEPOSIT = "Invalid deposit requested";

    private @Autowired UserService userService;
    private @Autowired TransactionService transactionService;

    private @Autowired AccountRepository accountRepository;
    private @Autowired DepositRepository depositRepository;
    private @Autowired PreciousMetalRepository preciousMetalRepository;
    private @Autowired InvestmentPortfolioRepository investmentRepository;
    private @Autowired PreciousMetalPriceRepository preciousMetalPriceRepository;
    private @Autowired PreciousMetalPricesRetriever preciousMetalPricesRetriever;
    private @Autowired FinancialInstitutionRepository financialInstitutionRepository;
    private @Autowired PreciousMetalProductRepository preciousMetalProductRepository;
    private @Autowired PreciousMetalTransactionRepository preciousMetalTransactionRepository;

    @Transactional(readOnly = true)
    public @Override Optional<InvestmentPortfolio> retrievePortfolio() {
        final Optional<User> user = userService.currentUser();
        return user.isPresent() ? Optional.of(retrievePortfolio(user.get())) : Optional.empty();
    }

    private InvestmentPortfolio retrievePortfolio(final User user) {
        return investmentRepository.findByOwner(user);
    }

    public @Override Optional<Deposit> save(final DepositDTO deposit) {
        Deposit saved = null;
        if (deposit != null) {
            final Optional<User> user = userService.currentUser();
            if (user.isPresent()) {
                final InvestmentPortfolio portfolio = retrievePortfolio(user.get());
                if (hasText(deposit.getUuid())) {
                    saved = merge(deposit, portfolio);
                } else {
                    saved = create(deposit, portfolio, user.get());
                }
                if (saved != null) {
                    saved = depositRepository.save(saved);
                    transferMoney(deposit, saved);
                }
            }
        }
        return Optional.ofNullable(saved);
    }

    private Deposit create(final DepositDTO deposit, final InvestmentPortfolio portfolio, final User user) {
        final Deposit created = new Deposit();
        created.setPortfolio(portfolio);
        created.setName(deposit.getName());
        created.setOpened(deposit.getOpened());
        created.setDuration(deposit.getDuration());
        setTransaction(created, user, deposit.getBalance());
        created.setAnnualPercentageYield(deposit.getAnnualPercentageYield());
        created.setFinancialInstitution(financialInstitutionRepository.getOne(deposit.getFinancialInstitution()));
        return created;
    }

    private void setTransaction(final Deposit deposit, final User user, final BigDecimal value) {
        final MonetaryAmount amount = this.setTransaction(deposit, user, value, deposit.getOpened(), DepositTransactionType.CONTRACT);
        deposit.setBalance(amount);
    }

    private MonetaryAmount setTransaction(final Deposit deposit, final User user, final BigDecimal value, final Date date, final DepositTransactionType type) {
        final DepositTransaction transaction = new DepositTransaction();
        final List<DepositTransaction> transactions = hasText(deposit.getUuid()) ? deposit.getTransactions() : new ArrayList<>();
        transactions.add(transaction);
        deposit.setTransactions(transactions);
        final MonetaryAmount amount = type.equals(DepositTransactionType.WITHDRAWAL) ? deposit.getBalance().negate() : Money.of(user.getCurrency(), value);
        transaction.setAmount(amount);
        transaction.setDeposit(deposit);
        transaction.setOperationDate(date);
        transaction.setTransactionType(type);
        return amount;
    }

    private void transferMoney(final DepositDTO deposit, final Deposit created) {
        if (hasText(deposit.getAccount())) {
            addTransaction(deposit.getAccount(), created.getBalance().negate(), created.getOpened(), "account.deposit.transfer");
        }
    }

    @Transactional(readOnly = true)
    public @Override Optional<Deposit> findDeposit(final String uuid) {
        Deposit loaded = hasText(uuid) ? depositRepository.findOne(uuid) : null;
        if (loaded != null) {
            final Optional<User> user = userService.currentUser();
            if (!user.isPresent() || !loaded.getPortfolio().getOwner().equals(user.get())) {
                throw new IllegalArgumentException(InvestmentServiceImpl.INVALID_DEPOSIT);
            }
        }
        return Optional.ofNullable(loaded);
    }

    private Deposit merge(final DepositDTO deposit, final InvestmentPortfolio portfolio) {
        Deposit merge = null;
        final Optional<Deposit> loaded = findDeposit(deposit.getUuid());
        if (loaded.isPresent()) {
            merge = loaded.get();
            merge.setName(deposit.getName());
            merge.setOpened(deposit.getOpened());
            merge.setDuration(deposit.getDuration());
            merge.setAnnualPercentageYield(deposit.getAnnualPercentageYield());
            if (merge.getBalance().getNumber().doubleValueExact() != deposit.getBalance().doubleValue()) {
                merge.setBalance(Money.of(merge.getBalance().getCurrency(), deposit.getBalance()));
                final DepositTransaction buy = merge.getTransactions().get(0);
                buy.setAmount(merge.getBalance());
                buy.setOperationDate(merge.getOpened());
            }
        }
        return merge;
    }

    public @Override Optional<Deposit> cancel(final DepositDTO dto) {
        Optional<Deposit> loaded = findDeposit(dto.getUuid());
        if (loaded.isPresent()) {
            Deposit deposit = loaded.get();
            boolean totalCancellation = dto.getBalance().abs().compareTo(deposit.getBalance().getNumber().numberValue(BigDecimal.class)) >= 0;
            final MonetaryAmount amount = setTransaction(deposit, deposit.getPortfolio().getOwner(), dto.getBalance(), dto.getOpened(), totalCancellation ? DepositTransactionType.WITHDRAWAL : DepositTransactionType.PARTIAL_CANCELLATION);
            deposit.setBalance(deposit.getBalance().add(amount));
            if (hasText(dto.getAccount())) {
                addTransaction(dto.getAccount(), amount.negate(), dto.getOpened(), "deposit.cancellation.transfer");
            }
            if ((dto.getInterest() != null) && (dto.getInterest().compareTo(BigDecimal.ZERO) > 0)) {
                final MonetaryAmount interest = Money.of(deposit.getBalance().getCurrency(), dto.getInterest());
                deposit.setInterest(deposit.getInterest().add(interest));
                if (hasText(dto.getInterestAccount())) {
                    addTransaction(dto.getInterestAccount(), interest, dto.getOpened(), "deposit.interest.transfer", "TRANSFER", "INTEREST");
                }
            }
            if (totalCancellation) {
                deposit.setVisible(false);
            }
            deposit = depositRepository.save(deposit);
            loaded = Optional.ofNullable(deposit.isVisible() ? deposit : null);
        }
        return loaded;
    }

    private void addTransaction(final String account, final MonetaryAmount amount, final Date date, final String memo, final String... tags) {
        final TransactionDTO dto = new TransactionDTO();
        dto.setTags(tags);
        dto.setMemo(memo);
        dto.setAccount(account);
        dto.setOperationDate(date);
        dto.setTransactionType(TransactionType.TRANSFER);
        dto.setAmount(amount.getNumber().numberValue(BigDecimal.class));
        transactionService.save(dto);
    }

    public @Override Optional<PreciousMetalProduct> save(final PreciousMetalProduct product) {
        PreciousMetalProduct created = null;
        if ((product != null) && (!hasText(product.getUuid()))) {
            created = preciousMetalProductRepository.save(product);
        }
        return Optional.ofNullable(created);
    }

    @Transactional(readOnly = true)
    public @Override Collection<PreciousMetalProduct> obtainPreciousMetalProducts() {
        return preciousMetalProductRepository.findAll();
    }

    public @Override Optional<PreciousMetal> buyPreciousMetal(final PreciousMetalTransactionDTO transaction) {
        PreciousMetal preciousMetal = null;
        if ((transaction != null) && (hasText(transaction.getProduct()))) {
            final PreciousMetalProduct product = preciousMetalProductRepository.findOne(transaction.getProduct());
            if (product != null) {
                preciousMetal = getOrCreatePreciousMetal(product, transaction);
                if (preciousMetal != null) {
                    preciousMetal = buyPreciousMetal(product, transaction, preciousMetal);
                    transferPreciousMetalAmount(transaction);
                }
            }
        }
        return Optional.ofNullable(preciousMetal);
    }

    private void transferPreciousMetalAmount(final PreciousMetalTransactionDTO transaction) {
        if (hasText(transaction.getAccount())) {
            addTransaction(transaction.getAccount(), transaction.getMoney().negate(), transaction.getOperationDate(), "account.pm.transfer" + (transaction.getMoney().isNegative() ? ".sell" : ""));
        }
    }

    private PreciousMetal getOrCreatePreciousMetal(final PreciousMetalProduct product, final PreciousMetalTransactionDTO transaction) {
        PreciousMetal preciousMetal = null;
        final Optional<User> user = userService.currentUser();
        if (user.isPresent()) {
            preciousMetal = preciousMetalRepository.findPreciousMetal(user.get(), product.getMetalType());
            if (preciousMetal == null) {
                preciousMetal = createPreciousMetal(user.get(), product, transaction);
            }
        }
        return preciousMetal;
    }

    private PreciousMetal createPreciousMetal(final User user, final PreciousMetalProduct product, final PreciousMetalTransactionDTO transaction) {
        final InvestmentPortfolio portfolio = retrievePortfolio(user);
        final PreciousMetal preciousMetal = new PreciousMetal();
        preciousMetal.setPortfolio(portfolio);
        preciousMetal.setPreciousMetalType(product.getMetalType());
        preciousMetal.setWeight(transaction.getWeightInMetal().floatValue());
        return preciousMetalRepository.save(preciousMetal);
    }

    private PreciousMetal buyPreciousMetal(final PreciousMetalProduct product, final PreciousMetalTransactionDTO transaction, final PreciousMetal preciousMetal) {
        final PreciousMetalTransaction preciousMetalTransaction = transaction.asTransaction(preciousMetal, product);
        if (!CollectionUtils.isEmpty(preciousMetal.getTransactions())) {
            preciousMetal.setWeight(preciousMetal.getWeight() + transaction.getWeightInMetal().floatValue());
        }
        preciousMetal.addTransaction(preciousMetalTransactionRepository.save(preciousMetalTransaction));
        preciousMetalRepository.save(preciousMetal);
        return preciousMetal;
    }

    @Cacheable(OssMoneyApplication.PRICES_CACHE)
    public @Override List<Optional<PreciousMetalPrice>> retrievePreciousMetalPrices() {
        final List<Optional<PreciousMetalPrice>> metals = new ArrayList<>(4);
        metals.add(retrievePreciousMetalPrice(PreciousMetalType.GOLD));
        metals.add(retrievePreciousMetalPrice(PreciousMetalType.SILVER));
        metals.add(retrievePreciousMetalPrice(PreciousMetalType.PLATINUM));
        metals.add(retrievePreciousMetalPrice(PreciousMetalType.PALLADIUM));
        return metals;
    }

    private Optional<PreciousMetalPrice> retrievePreciousMetalPrice(final PreciousMetalType preciousMetalType) {
        final Optional<PreciousMetalPrice> price = preciousMetalPricesRetriever.retrievePreciousMetalPrice(preciousMetalType);
        return price.isPresent() ? price : Optional.ofNullable(preciousMetalPriceRepository.findTopByPreciousMetalTypeOrderByDateDesc(preciousMetalType));
    }

    public @Override Optional<PreciousMetal> sellPreciousMetal(final PreciousMetalTransactionDTO transaction) {
        PreciousMetal metal = null;
        if ((transaction != null) && (hasText(transaction.getUuid()))) {
            final PreciousMetalTransaction loaded = preciousMetalTransactionRepository.findOne(transaction.getUuid());
            if (loaded != null) {
                metal = sellPreciousMetal(loaded, transaction);
                transaction.setCurrency(loaded.getAmount().getCurrency().getCurrencyCode());
                transferPreciousMetalAmount(transaction);
            }
        }
        return Optional.ofNullable(metal);
    }

    private PreciousMetal sellPreciousMetal(final PreciousMetalTransaction transaction, final PreciousMetalTransactionDTO data) {
        PreciousMetal metal = transaction.getPreciousMetal();
        int remaining = transaction.getRemaining();
        int sold = remaining >= data.getQuantity() ? data.getQuantity() : remaining;
        if (sold > 0) {
            transaction.setRemaining(remaining - sold);
            transaction.setVisible(transaction.getRemaining() > 0);
            preciousMetalTransactionRepository.save(transaction);
            metal.setWeight(metal.getWeight() - (sold * (transaction.getWeightInMetal() / transaction.getQuantity())));
            metal.setVisible(metal.getWeight() > 0);
            preciousMetalRepository.save(metal);
        }
        return metal.isVisible() ? metal : null;
    }

}
