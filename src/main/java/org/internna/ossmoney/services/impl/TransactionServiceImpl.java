package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.Optional;
import java.math.BigDecimal;
import java.util.Collections;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.internna.ossmoney.util.MoneyUtils;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.model.transaction.Tags;
import org.internna.ossmoney.data.AccountRepository;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.model.account.AccountType;
import org.internna.ossmoney.data.TransactionRepository;
import org.internna.ossmoney.services.TransactionService;
import org.internna.ossmoney.model.transaction.Transaction;
import org.internna.ossmoney.model.transaction.ReconcileDTO;
import org.internna.ossmoney.model.transaction.TransactionDTO;
import org.internna.ossmoney.model.transaction.TransactionType;

import static org.internna.ossmoney.util.StringUtils.hasText;

@Service
@Transactional
public final class TransactionServiceImpl implements TransactionService {

    private static final String INVALID_ACCOUNT = "Invalid account requested";

    private @Autowired UserService userService;
    private @Autowired AccountService accountService;
    private @Autowired AccountRepository accountRepository;
    private @Autowired TransactionRepository transactionRepository;

    @Transactional(readOnly = true)
    public @Override Optional<Transaction> find(final String uuid) {
        final Transaction transaction = hasText(uuid) ? visible(transactionRepository.findOne(uuid)) : null;
        if (transaction != null) {
            final Optional<User> user = userService.currentUser();
            if (!user.isPresent() || !transaction.getAccount().getOwner().equals(user.get())) {
                throw new IllegalArgumentException(TransactionServiceImpl.INVALID_ACCOUNT);
            }
        }
        return Optional.ofNullable(transaction);
    }

    private Transaction visible(final Transaction transaction) {
        return (transaction != null) && transaction.isVisible() && transaction.getAccount().isVisible() ? transaction : null;
    }

    @Transactional(readOnly = true)
    public @Override List<Transaction> incomeAndExpenses(final Date date) {
        final Optional<User> user = userService.currentUser();
        final List<Transaction> found = user.isPresent() ? transactionRepository.findByOperationDate(date, user.get(), AccountType.values(), TransactionType.INCOME, TransactionType.EXPENSE) : Collections.emptyList();
        return CollectionUtils.isEmpty(found) ? Collections.emptyList() : Collections.unmodifiableList(found);
    }

    @Transactional(readOnly = true)
    public @Override List<Transaction> obtainTransactions(final String accountId, final Date from, final Date to) {
        final Optional<User> user = userService.currentUser();
        final List<Transaction> transactions = user.isPresent() ? transactionRepository.findByInterval(accountRepository.getOne(accountId), from, to, user.get()) : null;
        return CollectionUtils.isEmpty(transactions) ? Collections.emptyList() : Collections.unmodifiableList(transactions);
    }

    @Transactional
    public @Override Optional<Account> delete(final String uuid) {
        Account account = null;
        final Optional<Transaction> loaded = find(uuid);
        if (loaded.isPresent()) {
            final Transaction transaction = loaded.get();
            account = transaction.getAccount();
            transaction.setVisible(false);
            transactionRepository.save(transaction);
            account.addBalance(transaction.getAmount().negate());
            accountRepository.save(account);
        }
        return Optional.ofNullable(account);
    }

    @Transactional
    public @Override Account[] reconcile(final ReconcileDTO reconciliation) {
        final Optional<Account> loaded = accountService.find(reconciliation.getAccount());
        final Optional<Account> chargeAccount = accountService.find(reconciliation.getChargeAccount());
        final Account[] accounts = new Account[chargeAccount.isPresent() ? 2 : 1];
        if (loaded.isPresent()) {
            BigDecimal amount = BigDecimal.ZERO;
            final Account account = loaded.get();
            for (Transaction transaction : transactionRepository.findTransactions(account, reconciliation.getTransactions())) {
                amount = amount.add(transaction.getAmount().getNumber().numberValue(BigDecimal.class));
                transaction.setReconciled(true);
                transactionRepository.save(transaction);
            }
            accounts[0] = account;
            chargeAccounts(Money.of(account.getCurrentBalance().getCurrency(), amount), accounts, chargeAccount);
        }
        return accounts;
    }

    private void chargeAccounts(final MonetaryAmount amount, final Account[] accounts, final Optional<Account> destination) {
        accounts[0].addBalance(amount.negate());
        accountRepository.save(accounts[0]);
        if (destination.isPresent()) {
            final Account chargeAccount = destination.get();
            if (accounts.length > 1) {
                accounts[1] = chargeAccount;
            }
            chargeAccount.addBalance(amount);
            accountRepository.save(chargeAccount);
            final Transaction transaction = new Transaction();
            transaction.setAmount(amount);
            transaction.setAccount(chargeAccount);
            transaction.setOperationDate(new Date());
            transaction.setMemo("credit.card.reconciliation");
            transaction.setTags(Arrays.asList(Tags.TRANSFER));
            transaction.setTransactionType(TransactionType.TRANSFER);
            transactionRepository.save(transaction);
        }
    }

    @Transactional
    public @Override Optional<Transaction> save(final TransactionDTO dto) {
        Transaction created = null;
        if (dto != null) {
            Optional<Transaction> transaction = hasText(dto.getUuid()) ? merge(dto) : create(dto);
            if (transaction.isPresent()) {
                created = transactionRepository.save(transaction.get());
                chargeAccounts(created.getAmount().negate(), new Account[] {created.getAccount()}, accountService.find(dto.getChargeAccount()));
            }
        }
        return Optional.ofNullable(created);
    }

    private Optional<Transaction> create(final TransactionDTO dto) {
        Transaction created = null;
        final Optional<Account> account = accountService.find(dto.getAccount());
        if (account.isPresent()) {
            created = dto.asTransaction(account.get());
        }
        return Optional.ofNullable(created);
    }

    private Optional<Transaction> merge(final TransactionDTO dto) {
        final Optional<Transaction> loaded = find(dto.getUuid());
        if (loaded.isPresent()) {
            final Transaction transaction = loaded.get();
            transaction.setMemo(dto.getMemo());
            transaction.setTags(dto.getTags());
            transaction.setOperationDate(dto.getOperationDate());
            transaction.setReferenceNumber(dto.getReferenceNumber());
            transaction.setTransactionType(dto.getTransactionType());
            if (!MoneyUtils.equals(transaction.getAmount(), dto.getAmount())) {
                final Account account = transaction.getAccount();
                final MonetaryAmount amount = Money.of(account.getCurrentBalance().getCurrency(), dto.getAmount());
                account.addBalance(transaction.getAmount().negate());
                accountRepository.save(account);
                transaction.setAmount(amount);
            }
        }
        return loaded;
    }

}
