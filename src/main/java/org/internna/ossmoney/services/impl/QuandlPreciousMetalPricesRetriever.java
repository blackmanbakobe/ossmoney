package org.internna.ossmoney.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.javamoney.moneta.Money;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import javax.money.MonetaryAmount;
import javax.money.MonetaryCurrencies;
import org.internna.ossmoney.util.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.internna.ossmoney.data.PreciousMetalRepository;
import org.internna.ossmoney.data.PreciousMetalPriceRepository;
import org.internna.ossmoney.services.PreciousMetalPricesRetriever;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;
import org.internna.ossmoney.model.investment.pm.PreciousMetalPrice;

@Service
public class QuandlPreciousMetalPricesRetriever implements PreciousMetalPricesRetriever {

    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private static final String QUANDL_GLD_URL = "https://www.quandl.com/api/v1/datasets/LBMA/GOLD.json?trim_start=";
    private static final String QUANDL_PAL_URL = "https://www.quandl.com/api/v1/datasets/LPPM/PALL.json?trim_start=";
    private static final String QUANDL_PLA_URL = "https://www.quandl.com/api/v1/datasets/LPPM/PLAT.json?trim_start=";
    private static final String QUANDL_SIL_URL = "https://www.quandl.com/api/v1/datasets/OFDP/SILVER_5.json?trim_start=";

    private @Autowired PreciousMetalRepository preciousMetalRepository;
    private @Autowired PreciousMetalPriceRepository preciousMetalPriceRepository;

    public @Override Optional<PreciousMetalPrice> retrievePreciousMetalPrice(final PreciousMetalType preciousMetalType) {
        switch (preciousMetalType) {
        case GOLD:
            return retrieveGoldPrice();
        case SILVER:
            return retrieveSilverPrice();
        case PLATINUM:
            return retrievePlatinumPrice();
        default:
            return retrievePalladiumPrice();
        }
    }

    public @Override Optional<PreciousMetalPrice> retrieveGoldPrice() {
        return retrieveMetalPrice(QUANDL_GLD_URL, PreciousMetalType.GOLD, 1, 3, 5);
    }

    public @Override Optional<PreciousMetalPrice> retrieveSilverPrice() {
        return retrieveMetalPrice(QUANDL_SIL_URL, PreciousMetalType.SILVER, 1, 2, 3);
    }

    public @Override Optional<PreciousMetalPrice> retrievePlatinumPrice() {
        return retrieveMetalPrice(QUANDL_PLA_URL, PreciousMetalType.PLATINUM, 1, 3, 5);
    }

    public @Override Optional<PreciousMetalPrice> retrievePalladiumPrice() {
        return retrieveMetalPrice(QUANDL_PAL_URL, PreciousMetalType.PALLADIUM, 1, 3, 5);
    }

    private Optional<PreciousMetalPrice> retrieveMetalPrice(final String url, final PreciousMetalType preciousMetalType, int... positions) {
        Date date = null;
        PreciousMetalPrice price = null;
        try {
            date = new Date();
            final Date from = DateUtils.add(date, -15);
            price = preciousMetalPriceRepository.findByPreciousMetalTypeAndDate(preciousMetalType, formatter.parse(formatter.format(date)));
            if (price == null) {
                final QuandlQueryResponse spot = new RestTemplate().getForObject(url + formatter.format(from), QuandlQueryResponse.class);
                if ((spot != null) && (spot.getData() != null) && (spot.getData().length > 0) && (spot.getData()[0] != null) && (spot.getData()[0].length > 0)) {
                    final String[] data = spot.getData()[0];
                    date = formatter.parse(data[0]);
                    price = preciousMetalPriceRepository.findByPreciousMetalTypeAndDate(preciousMetalType, date);
                    if (price == null) {
                        price = new PreciousMetalPrice();
                        price.setDate(date);
                        price.setSpot_usd(amount(Locale.US, data[positions[0]]));
                        price.setSpot_gbp(amount(Locale.UK, data[positions[1]]));
                        price.setSpot_eur(amount(Locale.GERMANY, data[positions[2]]));
                        price.setPreciousMetalType(preciousMetalType);
                        price = preciousMetalPriceRepository.save(price);
                    }
                }
            }
        } catch (Exception ex) {
            logger.warn("Could not parse date: {}", ex.getMessage());
        }
        return Optional.ofNullable(price);
    }

    private MonetaryAmount amount(final Locale currency, final String amount) {
        final BigDecimal perGram = new BigDecimal(amount).divide(new BigDecimal("31.1034768"), 5, RoundingMode.CEILING);
        return Money.of(MonetaryCurrencies.getCurrency(currency), perGram);
    }

    public static final class QuandlQueryResponse {
        private String[][] data;

        public String[][] getData() {
            return data;
        }

        public void setData(final String[][] data) {
            this.data = data;
        }

    }

}
