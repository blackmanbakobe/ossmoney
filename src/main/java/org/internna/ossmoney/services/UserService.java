package org.internna.ossmoney.services;

import java.util.Optional;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.security.UserDTO;

public interface UserService {

    Optional<User> currentUser();

    Optional<User> update(UserDTO user);

}
