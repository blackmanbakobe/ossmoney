package org.internna.ossmoney.services;

import java.util.Optional;
import java.util.Collection;
import org.internna.ossmoney.model.budget.Budget;
import org.internna.ossmoney.model.budget.BudgetDTO;

public interface BudgetService {

    Boolean remove(String uuid);

    Optional<Budget> find(String uuid);

    Collection<Budget> retrieveBudget();

    Optional<Budget> save(BudgetDTO budget);

}
