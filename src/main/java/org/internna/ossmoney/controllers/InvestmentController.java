package org.internna.ossmoney.controllers;

import java.util.Optional;
import java.util.Collection;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.pm.PreciousMetal;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct;
import org.internna.ossmoney.model.investment.pm.PreciousMetalTransactionDTO;

@Controller
@RequestMapping(value = "/investment")
public final class InvestmentController {

    private @Autowired InvestmentService investmentService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody Optional<InvestmentPortfolio> retrievePortfolio() {
        return investmentService.retrievePortfolio();
    }

    @RequestMapping(value = "deposit", method = RequestMethod.POST)
    public @ResponseBody Optional<Deposit> save(@RequestBody final DepositDTO deposit) {
        return investmentService.save(deposit);
    }

    @RequestMapping(value = "deposit/cancel", method = RequestMethod.POST)
    public @ResponseBody Optional<Deposit> cancel(@RequestBody final DepositDTO deposit) {
        return investmentService.cancel(deposit);
    }

    @ResponseBody
    @RequestMapping(value = "/pm/products", method = RequestMethod.GET)
    public Collection<PreciousMetalProduct> products() {
        return investmentService.obtainPreciousMetalProducts();
    }

    @RequestMapping(value = "/pm", method = RequestMethod.POST)
    public @ResponseBody Optional<PreciousMetal> buyPreciousMetal(@RequestBody final PreciousMetalTransactionDTO transaction) {
        return investmentService.buyPreciousMetal(transaction);
    }

    @RequestMapping(value = "/pm/sell", method = RequestMethod.POST)
    public @ResponseBody Optional<PreciousMetal> sellPreciousMetal(@RequestBody final PreciousMetalTransactionDTO transaction) {
        return investmentService.sellPreciousMetal(transaction);
    }

}
