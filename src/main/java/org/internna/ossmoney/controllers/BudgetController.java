package org.internna.ossmoney.controllers;

import java.util.Optional;
import java.util.Collection;
import org.internna.ossmoney.model.budget.Budget;
import org.internna.ossmoney.services.BudgetService;
import org.internna.ossmoney.model.budget.BudgetDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/budget")
public final class BudgetController {

    private @Autowired BudgetService budgetService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public Collection<Budget> obtainAccounts() {
        return budgetService.retrieveBudget();
    }

    @ResponseBody
    @RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE)
    public Boolean delete(@PathVariable("uuid") final String uuid) {
        return budgetService.remove(uuid);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public Optional<Budget> save(@RequestBody final BudgetDTO budget) {
        return budgetService.save(budget);
    }

}
