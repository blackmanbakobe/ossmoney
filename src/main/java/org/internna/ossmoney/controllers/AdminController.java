package org.internna.ossmoney.controllers;

import java.util.Optional;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.model.account.FinancialInstitution;
import org.internna.ossmoney.model.account.FinancialInstitutionDTO;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct;

@Controller
@RequestMapping("/admin")
public final class AdminController {

    private @Autowired AccountService accountService;
    private @Autowired InvestmentService investmentService;

    @ResponseBody
    @RequestMapping(value = "/banks", method = RequestMethod.POST)
    public Optional<FinancialInstitution> create(@RequestBody final FinancialInstitutionDTO bank) {
        return accountService.save(bank.asFinancialInstitution());
    }

    @ResponseBody
    @RequestMapping(value = "/metals", method = RequestMethod.POST)
    public Optional<PreciousMetalProduct> createProduct(@RequestBody final PreciousMetalProduct metal) {
        return investmentService.save(metal);
    }

}
