package org.internna.ossmoney.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.model.transaction.Tags;
import org.internna.ossmoney.services.TransactionService;
import org.internna.ossmoney.model.transaction.Transaction;
import org.internna.ossmoney.model.transaction.ReconcileDTO;
import org.internna.ossmoney.model.transaction.TransactionDTO;
import org.springframework.stereotype.Controller;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Controller
@RequestMapping(value = "/transaction")
public final class TransactionController {

    private @Autowired TransactionService transactionService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public Optional<Transaction> save(@RequestBody final TransactionDTO transaction) {
        return transactionService.save(transaction);
    }

    @ResponseBody
    @RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE)
    public Optional<Account> delete(@PathVariable("uuid") final String uuid) {
        return transactionService.delete(uuid);
    }

    @ResponseBody
    @RequestMapping(value = "/reconciliation", method = RequestMethod.POST)
    public Account[] reconcile(@RequestBody final ReconcileDTO reconciliation) {
        return transactionService.reconcile(reconciliation);
    }

    @ResponseBody
    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public Tags[] obtainTags() {
        return Tags.values();
    }

    @ResponseBody
    @RequestMapping(value = "/income-and-expenses", method = RequestMethod.GET)
    public Iterable<Transaction> obtainIncomeAndExpensesForThisMonth() {
        return obtainIncomeAndExpensesForSeveralMonths(0);
    }

    @ResponseBody
    @RequestMapping(value = "/income-and-expenses/{months}", method = RequestMethod.GET)
    public Iterable<Transaction> obtainIncomeAndExpensesForSeveralMonths(@PathVariable(value = "months") final Integer months) {
        final Date today = new Date();
        return transactionService.incomeAndExpenses(DateUtils.add(today, getDays(today, months)));
    }

    @ResponseBody
    @RequestMapping(value = "/{account}/{from}/{to}", method = RequestMethod.GET)
    public List<Transaction> obtainTransactions(@PathVariable(value = "account") final String accountId, @PathVariable(value = "from") @DateTimeFormat(iso = ISO.DATE) final Date from, @PathVariable(value = "to") @DateTimeFormat(iso = ISO.DATE) final Date to) {
        return transactionService.obtainTransactions(accountId, from, to);
    }

    private int getDays(final Date today, final int months) {
        return months == 0 ? -DateUtils.getDayOfMonth(today) : months * -30;
    }

}
