package org.internna.ossmoney.data;

import java.util.Date;
import java.util.List;
import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.model.account.AccountType;
import org.internna.ossmoney.model.transaction.Tags;
import org.internna.ossmoney.model.transaction.Transaction;
import org.internna.ossmoney.model.transaction.TransactionType;

public interface TransactionRepository extends JpaRepository<Transaction, String> {

    @Query("SELECT t FROM Transaction t WHERE t.visible = TRUE AND t.account.visible = TRUE AND t.operationDate > ?1 AND t.account.owner = ?2 AND t.account.accountType IN ?3 AND t.transactionType IN ?4")
    List<Transaction> findByOperationDate(Date from, User user, AccountType[] accountType, TransactionType... types);

    @Query("SELECT t FROM Transaction t JOIN t.tags b WHERE t.visible = TRUE AND t.account.visible = TRUE AND t.transactionType = 'EXPENSE' AND t.operationDate > ?1 AND t.account.owner = ?2 AND t.account.accountType IN ?3 AND b = ?4")
    List<Transaction> findExpensesByDate(Date from, User user, AccountType[] accountType, Tags tag);

    @Query("SELECT t FROM Transaction t WHERE t.visible = TRUE AND t.reconciled = FALSE AND t.account = ?1 AND t.operationDate > ?2 AND t.operationDate <= ?3 AND t.account.owner = ?4 ORDER BY t.operationDate DESC")
    List<Transaction> findByInterval(Account account, Date from, Date to, User user);

    @Query(value = "SELECT SUM(AMOUNT) FROM TRANSACTION WHERE VISIBLE = TRUE AND RECONCILED = FALSE AND ACCOUNT = ?1", nativeQuery = true)
    BigDecimal sumTransactions(String uuid);

    @Query(value = "SELECT t FROM Transaction t WHERE t.visible = TRUE AND t.account = ?1 AND t.uuid IN ?2")
    Iterable<Transaction> findTransactions(Account account, List<String> transactions);

}
