package org.internna.ossmoney.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct;

public interface PreciousMetalProductRepository extends JpaRepository<PreciousMetalProduct, String> {

}
