package org.internna.ossmoney.data;

import org.internna.ossmoney.model.Country;
import org.springframework.data.repository.Repository;

public interface CountryRepository extends Repository<Country, String> {

    Country findOne(String uuid);

    Country findByCountryKey(String countryKey);

}
