package org.internna.ossmoney.data;

import org.internna.ossmoney.model.security.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {

}
