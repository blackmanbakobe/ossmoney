package org.internna.ossmoney;

import org.springframework.cache.CacheManager;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;

@EnableCaching
@SpringBootApplication
public class OssMoneyApplication {

    public static final String PRICES_CACHE = "prices";

    public static void main(final String[] args) {
        SpringApplication.run(OssMoneyApplication.class, args);
    }

    public @Bean CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(OssMoneyApplication.PRICES_CACHE);
    }

}
