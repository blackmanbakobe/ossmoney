package org.internna.ossmoney.config;

import org.internna.ossmoney.security.SocialSignInAdapter;
import org.internna.ossmoney.security.SocialConnectionSignUp;
import org.internna.ossmoney.security.CombinedUserDetailsService;
import org.springframework.social.UserIdSource;
import org.springframework.core.env.Environment;
import org.springframework.context.annotation.Bean;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;

@Configuration
public class SocialConfig implements SocialConfigurer {

	@Bean public CombinedUserDetailsService socialUserDetailsService() {
		return new CombinedUserDetailsService();
	}

	@Bean public SignInAdapter signInAdapter() {
		return new SocialSignInAdapter();
	}

	@Bean public ConnectionSignUp connectionSignUp() {
		return new SocialConnectionSignUp();
	}

	@Override public void addConnectionFactories(final ConnectionFactoryConfigurer configurer, final Environment environment) {
		configurer.addConnectionFactory(new GoogleConnectionFactory(
			environment.getProperty("spring.social.google.consumerKey"),
			environment.getProperty("spring.social.google.consumerSecret")
		));
	}

	@Override public final UserIdSource getUserIdSource() {
		return new AuthenticationNameUserIdSource();
	}

	@Override public final UsersConnectionRepository getUsersConnectionRepository(final ConnectionFactoryLocator connectionFactoryLocator) {
		final InMemoryUsersConnectionRepository repository = new InMemoryUsersConnectionRepository(connectionFactoryLocator);
		repository.setConnectionSignUp(connectionSignUp());
		return repository;
	}

}
