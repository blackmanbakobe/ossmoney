CREATE TABLE USERS(
    USERNAME VARCHAR(255) NOT NULL,
    SKIN VARCHAR(32) NOT NULL,
    LOCALE VARCHAR(32) NOT NULL,
    CURRENCY VARCHAR(3) NOT NULL,
    CREATED TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ADMINISTRATOR BOOLEAN NOT NULL DEFAULT FALSE
);

ALTER TABLE USERS ADD PRIMARY KEY (USERNAME);
