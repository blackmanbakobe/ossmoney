(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyInvestmentNetWorthController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.recalculateNetWorth = function() {
			$scope.data = [];
			$scope.labels = [];
			$scope.deposits = 0;
			$scope.preciousMetals = 0;
			if ($rootScope.netWorth !== 0) {
				$scope.data.push($rootScope.netWorth);
				$scope.labels.push($translate.instant("investment.cash"));
			}
			if ($rootScope.portfolio !== undefined) {
				$scope.deposits = $rootScope.portfolio.deposits.reduce(function(previousValue, deposit) {
					return previousValue + deposit.balance.number;
				}, 0);
				if ($scope.deposits !== 0) {
					$scope.data.push($scope.deposits);
					$scope.labels.push($translate.instant("investment.deposits.total"));
				}
				if ($rootScope.portfolio.preciousMetals && $rootScope.preciousMetalPrices) {
					$scope.preciousMetals = $rootScope.portfolio.preciousMetals.reduce(function(previousValue, preciousMetal) {
						var index = 0;
						switch (preciousMetal.preciousMetalType) {
							case "SILVER": index = 1; break;
							case "PLATINUM": index = 2; break;
							case "PALLADIUM": index = 3; break;
						}
						return previousValue + ($rootScope.preciousMetalPrices[index]["spot_" + $rootScope.currency.currencyCode.toLowerCase()].number * preciousMetal.weight);
					}, 0);
				}
				if ($scope.preciousMetals) {
					$scope.data.push($scope.preciousMetals);
					$scope.labels.push($translate.instant("investment.preciousMetal.total"));
				}
			}
			if ($scope.data.length === 0) {
				$scope.data.push(1);
				$scope.labels.push($translate.instant("investment.networth.no_data"));
			}
		};
		$rootScope.$on("networth-changed", function() {
			$scope.recalculateNetWorth();
		});
		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculateNetWorth();
		});
		$rootScope.$on("precious-metals-prices-changed", function() {
			$scope.recalculateNetWorth();
		});
		$scope.recalculateNetWorth();
	}]);

})(angular);