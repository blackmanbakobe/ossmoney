(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyDepositAddEditController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "FinancialInstitution", "Deposit", "Alert", "Array", "ISO", function($rootScope, $scope, $state, $stateParams, $translate, FinancialInstitution, Deposit, Alert, Array, ISO) {
		$scope.apyValidation = "";
		$scope.nameValidation = "";
		$scope.bankValidation = "";
		$scope.balanceValidation = "";
		$scope.country = "country." + $rootScope.locale;
		$scope.deposit = $stateParams.deposit || {
			"duration": 6,
			"opened": new Date(),
			"annualPercentageYield": 0,
			"balance": {
				"number": 0,
				"currency": $rootScope.currency
			}
		};
		$scope.saveDeposit = function() {
			var depo = angular.copy($scope.deposit);
			delete depo.transactions;
			depo.opened = ISO.asDate(depo.opened);
			depo.balance = Number(depo.balance.number);
			depo.financialInstitution = depo.financialInstitution.uuid;
			depo.account = depo.account !== undefined ? depo.account.uuid : null;
			$scope.nameValidation = !$scope.deposit.name ? "has-error has-feedback" : "";
			$scope.bankValidation = !$scope.deposit.financialInstitution ? "has-error has-feedback" : "";
			$scope.balanceValidation = ($scope.deposit.balance.number === null) || ($scope.deposit.balance.number <= 0) ? "has-error has-feedback" : "";
			$scope.apyValidation = ($scope.deposit.annualPercentageYield === null) || ($scope.deposit.annualPercentageYield <= 0) ? "has-error has-feedback" : "";
			if (!$scope.nameValidation & !$scope.balanceValidation & !$scope.bankValidation & !$scope.apyValidation) {
				var deposit = Deposit.save(depo, function() {
					if (deposit && deposit.uuid) {
						var account = Array.find($rootScope.accounts, depo.account);
						if (account >= 0) {
							$rootScope.transactions[$rootScope.accounts[account].uuid] = [];
							$rootScope.accounts[account].currentBalance.number -= depo.balance;
							$rootScope.$broadcast("accounts-changed");
						}
						if ($scope.deposit.uuid !== undefined) {
							Array.remove($rootScope.portfolio.deposits, deposit.uuid);
						}
						$rootScope.portfolio.deposits.push(deposit);
						$rootScope.$broadcast("portfolio-changed");
						$state.go("account_main");
						Alert.show($translate.instant($scope.deposit.uuid !== undefined ? "deposit.modify.success" : "deposit.add.success"));
					} else {
						Alert.warn($translate.instant($scope.deposit.uuid !== undefined ? "deposit.modify.error" : "deposit.add.error"));
					}
				}, function(err) {
					Alert.warn($translate.instant($scope.deposit.uuid !== undefined ? "deposit.modify.error" : "deposit.add.error"));
				});
			}
		};
		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.substituteFinancialInstitution = function() {
			angular.forEach($rootScope.banks[$scope.country], function(bank) {
				if (bank.uuid === $scope.deposit.financialInstitution.uuid) {
					$scope.deposit.financialInstitution = bank;
				}
			});
		};
		$scope.findFinancialInstitutions = function(country, callback) {
			var banks = FinancialInstitution.query({
				"country": country
			}, function() {
				$rootScope.banks[country] = banks;
				callback(banks);
			});
		};
		if ($rootScope.banks[$scope.country] === undefined) {
			$scope.findFinancialInstitutions($scope.country, function(banks) {
				if ($scope.deposit.uuid === undefined) {
					$scope.deposit.financialInstitution = banks.length > 0 ? banks[0] : null;
				} else {
					$scope.substituteFinancialInstitution();
				}
			});
		} else if ($scope.deposit.uuid === undefined) {
			$scope.deposit.financialInstitution = $rootScope.banks[$scope.country].length > 0 ? $rootScope.banks[$scope.country][0] : null;
		} else {
			$scope.substituteFinancialInstitution();
		}
	}]);

})(angular);