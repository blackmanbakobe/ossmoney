(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyInvestmentBuyPreciousMetalController", ["$rootScope", "$scope", "$state", "$translate", "PreciousMetalTransaction", "PreciousMetalProduct", "Array", "Alert", function($rootScope, $scope, $state, $translate, PreciousMetalTransaction, PreciousMetalProduct, Array, Alert) {
		$scope.opened = false;
		$scope.loading = false;
		$scope.retrieving = false;
		$scope.mint = null;
		$scope.products = [];
		$scope.today = new Date();
		$scope.metalShape = "COIN";
		$scope.dateValidation = "";
		$scope.amountValidation = "";
		$scope.weightValidation = "";
		$scope.productValidation = "";
		$scope.quantityValidation = "";
		$scope.weightMetalValidation = "";
		$scope.transaction = {
			"amount": 0,
			"weight": 0,
			"quantity": 1,
			"purity": 0.9999,
			"weightInMetal": 0,
			"metalType": "GOLD",
			"transactionType": "BUY",
			"operationDate": new Date(),
			"currency": $rootScope.currency.currencyCode
		};

		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};

		$scope.recalculatePurity = function() {
			if ($scope.transaction.weight && $scope.transaction.weightInMetal) {
				var purity = Number(($scope.transaction.weightInMetal / $scope.transaction.weight).toFixed(4));
				$scope.transaction.purity = purity === 1 ? 0.9999 : purity;
			}
		};

		$scope.recalculateProducts = function() {
			$scope.products = [];
			$scope.transaction.product = null;
			angular.forEach($rootScope.preciousMetalProducts, function(product) {
				if ((!$scope.metalType || (product.metalType === $scope.metalType)) && (!$scope.metalShape || (product.shape === $scope.metalShape)) && (!$scope.mint || (product.mint === $scope.mint))) {
					$scope.products.push(product);
				}
			});
		};

		$scope.findPreciousMetalProducts = function() {
			$scope.retrieving = true;
			var products = PreciousMetalProduct.query(function() {
				$scope.retrieving = false;
				$rootScope.preciousMetalProducts = products;
				$scope.recalculateProducts();
			}, function(err) {
				Alert.warn($translate.instant("pm.products.error"));
			});
		};

		$scope.savePMTransaction = function() {
			$scope.amountValidation = !$scope.transaction.amount ? "has-error" : "";
			$scope.productValidation = !$scope.transaction.product ? "has-error" : "";
			$scope.dateValidation = !$scope.transaction.operationDate ? "has-error" : "";
			$scope.weightValidation = !$scope.transaction.weight || $scope.transaction.weight <= 0 ? "has-error" : "";
			$scope.quantityValidation = $scope.transaction.quantity === undefined || $scope.transaction.quantity === null || $scope.transaction.quantity <= 0 ? "has-error" : "";
			$scope.weightMetalValidation = !$scope.transaction.weightInMetal || $scope.transaction.weightInMetal <= 0 || $scope.transaction.weightInMetal > $scope.transaction.weight ? "has-error" : "";
			if (!$scope.productValidation && !$scope.amountValidation && !$scope.dateValidation && !$scope.weightValidation && !$scope.weightMetalValidation && !$scope.quantityValidation) {
				$scope.loading = true;
				var dto = angular.copy($scope.transaction);
				dto.product = dto.product.uuid;
				dto.account = dto.account ? dto.account.uuid : null;
				var preciousMetal = PreciousMetalTransaction.save(dto, function() {
					$scope.loading = false;
					if (preciousMetal && preciousMetal.uuid) {
						if ($rootScope.portfolio !== undefined) {
							Array.remove($rootScope.portfolio.preciousMetals, preciousMetal.uuid);
							$rootScope.portfolio.preciousMetals.push(preciousMetal);
							$rootScope.$broadcast("portfolio-changed");
						}
						var account = Array.find($rootScope.accounts, dto.account);
						if (account >= 0) {
							$rootScope.transactions[$rootScope.accounts[account].uuid] = [];
							$rootScope.accounts[account].currentBalance.number -= $scope.transaction.amount;
							$rootScope.$broadcast("accounts-changed");
						}
						$state.go("account_main");
						Alert.show($translate.instant("pm.buy.success"));
					} else {
						Alert.warn($translate.instant("pm.buy.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("pm.buy.error"));
				});
			}
		};

		if ($rootScope.preciousMetalProducts === undefined) {
			$scope.findPreciousMetalProducts();
		}

		$rootScope.$on("precious-metals-products-changed", function() {
			$scope.recalculateProducts();
		});

		$scope.$watch("mint", function() {
			$scope.recalculateProducts();
		});
		$scope.$watch("metalType", function() {
			$scope.recalculateProducts();
		});
		$scope.$watch("metalShape", function() {
			$scope.recalculateProducts();
		});

	}]);

})(angular);