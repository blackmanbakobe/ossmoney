(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyInvestmentSellPreciousMetalController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "PreciousMetalSellTransaction", "PreciousMetalProduct", "Array", "Alert", function($rootScope, $scope, $state, $stateParams, $translate, PreciousMetalSellTransaction, PreciousMetalProduct, Array, Alert) {
		$scope.products = [];
		$scope.opened = false;
		$scope.loading = false;
		$scope.retrieving = false;
		$scope.today = new Date();
		$scope.dateValidation = "";
		$scope.amountValidation = "";
		$scope.quantityValidation = "";
		$scope.metal = $stateParams.preciousMetal;
		$scope.transaction = angular.copy($stateParams.preciousMetalTransaction);

		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};

		$scope.recalculateProducts = function() {
			$scope.products = $rootScope.preciousMetalProducts;
			$scope.transaction.product = $scope.products[Array.find($scope.products, $scope.transaction.product.uuid)];
		};

		$scope.findPreciousMetalProducts = function() {
			$scope.retrieving = true;
			var products = PreciousMetalProduct.query(function() {
				$scope.retrieving = false;
				$rootScope.preciousMetalProducts = products;
				$scope.recalculateProducts();
			}, function(err) {
				Alert.warn($translate.instant("pm.products.error"));
			});
		};

		$scope.sellPreciousMetal = function() {
			$scope.amountValidation = !$scope.transaction.amount ? "has-error" : "";
			$scope.dateValidation = !$scope.transaction.operationDate ? "has-error" : "";
			$scope.quantityValidation = $scope.transaction.quantity === undefined || $scope.transaction.quantity === null || $scope.transaction.quantity <= 0 ? "has-error" : "";
			if (!$scope.amountValidation && !$scope.dateValidation && !$scope.quantityValidation) {
				$scope.loading = true;
				var dto = {
					"uuid" : $scope.transaction.uuid,
					"quantity": $scope.transaction.quantity,
					"amount": -$scope.transaction.amount.number,
					"operationDate": $scope.transaction.operationDate,
					"account": $scope.transaction.account ? $scope.transaction.account.uuid : null
				};
				var preciousMetal = PreciousMetalSellTransaction.save(dto, function() {
					$scope.loading = false;
					Array.remove($rootScope.portfolio.preciousMetals, $scope.metal);
					if (preciousMetal && preciousMetal.uuid) {
						$rootScope.portfolio.preciousMetals.push(preciousMetal);
					}
					var account = Array.find($rootScope.accounts, dto.account);
					if (account >= 0) {
						$rootScope.transactions[$rootScope.accounts[account].uuid] = [];
						$rootScope.accounts[account].currentBalance.number -= dto.amount;
						$rootScope.$broadcast("accounts-changed");
					}
					$rootScope.$broadcast("portfolio-changed");
					$state.go("account_main");
					Alert.show($translate.instant("pm.buy.success"));
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("pm.buy.error"));
				});
			}
		};

		if ($rootScope.preciousMetalProducts === undefined) {
			$scope.findPreciousMetalProducts();
		} else {
			$scope.products = $rootScope.preciousMetalProducts;
			$scope.transaction.product = $scope.products[Array.find($scope.products, $scope.transaction.product.uuid)];
		}

		$rootScope.$on("precious-metals-products-changed", function() {
			$scope.recalculateProducts();
		});

	}]);

})(angular);