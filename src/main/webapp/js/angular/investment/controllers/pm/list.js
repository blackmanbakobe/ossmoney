(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyInvestmentListPreciousMetalController", ["$rootScope", "$scope", "$state", "$timeout", "$translate", "Investment", "PreciousMetalPrices", function($rootScope, $scope, $state, $timeout, $translate, Investment, PreciousMetalPrices) {
		$scope.retrieving = false;
		$scope.rowCollection = [];
		$scope.recalculateTransactions = function() {
			$scope.metalTransactions = [];
			if ($rootScope.portfolio === undefined) {
				$scope.retrieving = true;
				var portfolio = Investment.query(function() {
					$scope.retrieving = false;
					$rootScope.portfolio = portfolio;
					$rootScope.$broadcast("portfolio-changed");
				});
			} else if ($rootScope.preciousMetalPrices === undefined) {
				$scope.retrieving = true;
				var prices = PreciousMetalPrices.query({}, function() {
					$scope.retrieving = false;
					$rootScope.preciousMetalPrices = prices;
					$scope.recalculateTransactions();
					$rootScope.$broadcast("precious-metals-prices-changed");
				});
			} else {
				angular.forEach($rootScope.portfolio.preciousMetals, function(preciousMetal) {
					angular.forEach(preciousMetal.transactions, function(transaction) {
						var index = 0;
						switch (preciousMetal.preciousMetalType) {
							case "SILVER": index = 1; break;
							case "PLATINUM": index = 2; break;
							case "PALLADIUM": index = 3; break;
						}
						transaction.preciousMetal = preciousMetal.uuid;
						transaction.translated = $translate.instant(transaction.product.name);
						transaction.mint = $translate.instant("pm." + transaction.product.mint);
						transaction.shape = $translate.instant("pm." + transaction.product.shape);
						transaction.metal = $translate.instant("pm." + transaction.product.metalType);
						transaction.spot = $rootScope.preciousMetalPrices[index]["spot_" + $rootScope.currency.currencyCode.toLowerCase()].number.toFixed(2);
						transaction.pnl = (100 * ((transaction.weightInMetal * transaction.spot) - transaction.amount.number) / transaction.amount.number).toFixed(2);
						$scope.metalTransactions.push(transaction);
					});
				});
			}
		};

		$scope.recalculateTransactions();

		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculateTransactions();
		});

	}]);

})(angular);