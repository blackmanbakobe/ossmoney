(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyPreciousMetalChartController", ["$rootScope", "$scope", "$translate", "PreciousMetalPrices", function($rootScope, $scope, $translate, PreciousMetalPrices) {
		$scope.recalculatePreciousMetals = function() {
			$scope.data = [];
			$scope.labels = [];
			$scope.preciousMetalsTotal = 0;
			if ($rootScope.preciousMetalPrices === undefined) {
				var prices = PreciousMetalPrices.query({}, function() {
					$rootScope.preciousMetalPrices = prices;
					$scope.recalculatePreciousMetals();
					$rootScope.$broadcast("precious-metals-prices-changed");
				});
			} else if ($rootScope.portfolio !== undefined) {
				angular.forEach($rootScope.portfolio.preciousMetals, function(preciousMetal) {
					var index = 0;
					switch (preciousMetal.preciousMetalType) {
						case "SILVER": index = 1; break;
						case "PLATINUM": index = 2; break;
						case "PALLADIUM": index = 3; break;
					}
					var preciousMetalPrice = $rootScope.preciousMetalPrices[index];
					var price = (preciousMetalPrice["spot_" + $rootScope.currency.currencyCode.toLowerCase()].number * preciousMetal.weight).toFixed(2);
					$scope.labels.push($translate.instant(preciousMetal.preciousMetalType));
					$scope.data.push(price);
					$scope.preciousMetalsTotal += Number(price);
				});
			}
			if ($scope.data.length === 0) {
				$scope.data.push(1);
				$scope.labels.push($translate.instant("investment.preciousMetal.no_data"));
			}
		};
		$scope.recalculatePreciousMetals();
		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculatePreciousMetals();
		});
	}]);

})(angular);