(function(angular) {

	var investment = angular.module("ossmoneyInvestment", ["ui.bootstrap", "chart.js", "ngResource", "ui.router", "pascalprecht.translate"]);

	investment.run(["$rootScope", "Investment", function($rootScope, Investment) {
		if ($rootScope.portfolio === undefined) {
			var portfolio = Investment.query(function() {
				$rootScope.portfolio = portfolio;
				$rootScope.$broadcast("portfolio-changed");
			});
		}
	}]);

})(angular);
