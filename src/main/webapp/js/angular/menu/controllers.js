(function(angular) {

	var menu = angular.module("ossmoneyMenu");

	menu.controller("OssmoneyMenuController", ["$scope", "$state", function($scope, $state) {
		$scope.menuOptions = [{
			"iconClass": "",
			"active": "active",
			"option" : "menu_account",
			"navigation" : "account"
		}, {
			"active": "",
			"iconClass": "",
			"option" : "menu_dashboard",
			"navigation" : "dashboard"
		}, {
			"active": "",
			"iconClass": "",
			"option" : "menu_budget",
			"navigation" : "budget"
		}, {
			"iconClass": "",
			"active": "",
			"option" : "menu_investment",
			"navigation" : "investment"
		}];
		$scope.switchOption = function(option) {
			angular.forEach($scope.menuOptions, function(anOption) {
				anOption.active = "";
			});
			option.active ="active";
			$state.go("blank", {
				"option": option.navigation
			});
		};
	}]);

})(angular);