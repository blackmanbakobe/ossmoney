(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"budget.add": "Añadir nuevo",
			"budget.categories": "Presupuestos",
			"budget.monthly.chart": "Cumplimiento:",
			"budget.actual": "Gastado",
			"budget.estimation": "Estimado",
			"budget.movement.title": "Gastos contabilizados",
			"budget.add.title": "Añadir presupuesto",
			"budget.add.name": "Nombre",
			"budget.add.amount": "Importe presupuestado",
			"budget.add.category": "Categoría",
			"budget.add.subcategory": "Etiqueta",
			"budget.add.validation.name": "Es necesario un identificador",
			"budget.add.validation.balance": "El importe es requerido",
			"budget.add.from": "Fecha de inicio",
			"budget.add.validation.from": "Indique una fecha de inicio",
			"budget.add.to": "Fecha de fin",
			"budget.add.validation.to": "Indique una fecha de fin",
			"budget.add.save": "Guardar presupuesto",
			"budget.save.success": "Presupuesto almacenado",
			"budget.save.error": "Error al almacenar el presupuesto",
			"budget.add.validation.tag": "La etiqueta es necesaria",
			"budget.empty": "No hay movimientos",
			"budget.edit.save": "Guardar cambios",
			"budget.edit.delete": "Eliminar presupuesto",
			"budget.delete.title": "Eliminar presupuesto",
			"budget.delete.type.message": "Escriba ELIMINAR para borrar el presupuesto",
			"budget.delete.warning": "Esta operación no puede deshacerse",
			"budget.delete.success": "Presupuesto eliminado",
			"budget.delete.error": "Error al eliminar el presupuesto",
			"budget.edit": "Datos del presupuesto",
			"budget.expenses.time": "Presupuesto en el tiempo"
		});
		$translateProvider.translations('en', {
			"budget.add": "Add budget",
			"budget.categories": "Categories",
			"budget.monthly.chart": "Budget monthly chart:",
			"budget.actual": "Spent",
			"budget.estimation": "Estimation",
			"budget.movement.title": "Allocated expenses",
			"budget.add.title": "Add budget",
			"budget.add.name": "Name",
			"budget.add.amount": "Amount",
			"budget.add.category": "Category",
			"budget.add.subcategory": "Tag",
			"budget.add.validation.name": "Name is required",
			"budget.add.validation.balance": "Amount is required",
			"budget.add.from": "From",
			"budget.add.validation.from": "Please provide a date",
			"budget.add.to": "To",
			"budget.add.validation.to": "Please provide a date",
			"budget.add.save": "Save budget",
			"budget.save.success": "Budget saved",
			"budget.save.error": "Error saving budget",
			"budget.add.validation.tag": "Please provide a tag",
			"budget.empty": "No movements",
			"budget.edit.save": "Save budget",
			"budget.edit.delete": "Delete budget",
			"budget.delete.title": "Delete budget",
			"budget.delete.type.message": "Type DELETE to remove this budget",
			"budget.delete.warning": "This action cannot be undone",
			"budget.delete.success": "Budget deleted",
			"budget.delete.error": "Error deleting budget",
			"budget.edit": "Budget data",
			"budget.expenses.time": "Budget over time"
		});
	}]);

})(angular);