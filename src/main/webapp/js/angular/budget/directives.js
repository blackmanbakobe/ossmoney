(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.directive("budgetRowGenerator", ["$compile", "$filter", "$translate", function($compile, $filter, $translate) {
	    var rowTemplate = '<div class="row" style="margin-bottom: 10px;padding: 3px">', colTemplate = '<div class="col-lg-4', span = '</span><span> / </span><span class="budget">';
	    return {
	    	restrict: "E",
	    	replace: true,
	        scope: {colCount: "="},
	        link: function(scope, element) {
	        	scope.renderBudgets = function(scope, element) {
			    	if (scope.$root.budgets) {
			    		var html = rowTemplate;
			            for (var i = 0; i < scope.$root.budgets.length; i++) {
			            	var budget = scope.$root.budgets[i];
			                html += colTemplate + (budget.spent > budget.amount.number ? ' surpassed">' : '">');
			                html += '<div class="text-center budget ellipsis">' + budget.name + '</div><div><a ui-sref="view_budget({ budget: ' + (i + 1) + ' })">';
			                html += '<div class="new-transaction-icon-inner"><span class="' + (budget.subcategory.icon ? budget.subcategory.icon : budget.subcategory.categories[0].icon) + '" aria-hidden="true" title="' + budget.name + '"></span></div>';
			                html += '</a></div><div class="text-center"><span' + (budget.spent && budget.spent > budget.amount.number ? ' class="red strong">' : '>') + (budget.spent || 0) + span;
			                html += $filter('currency')(budget.amount.number, $translate.instant(budget.amount.currency.currencyCode)) + '</span></div></div>';
			                if ((i % scope.colCount) ===  (scope.colCount - 1)) {
			                    html += '</div>' + rowTemplate;
			                }
			            }
			            if (scope.$root.budgets.length < 9) {
			                if ((scope.$root.budgets.length % scope.colCount) === 0) {
			                	html += '</div>' + rowTemplate;
			                }
			                html += '<div class="col-lg-4"><div class="text-center budget ellipsis">' + $translate.instant("budget.add");
			                html += '</div><div><a ui-sref="add_budget"><div class="new-transaction-icon-inner"><span>+</span></div></a></div></div>';
			                if ((scope.$root.budgets.length % scope.colCount) === 0) {
			                	html += '</div>';
			                }
			            }
			            html += '</div>';
			            element.html(html);
			            $compile(element.contents())(scope);
			    	}
			    }
	        	scope.$root.$watch("budgets.length", function() {
	        		scope.renderBudgets(scope, element);
	    		});
	        }
	    };
	}]);

})(angular);