(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetDeleteController", ["$scope", "$modal", function($scope, $modal) {
		$scope.showModal = function () {
			var modalInstance = $modal.open({
				"animation": "true",
				"templateUrl": "budget.remove.modal.html",
				"controller": 'OssmoneyBudgetDeleteModalController',
				"resolve": {
					"budget": function() {
						return $scope.budget;
					}
		        }
		    });
		};
	}]);

	budget.controller("OssmoneyBudgetDeleteModalController", ["$rootScope", "$scope", "$modalInstance", "$state", "$translate", "Alert", "Array", "Budget", "budget",  function($rootScope, $scope, $modalInstance, $state, $translate, Alert, Array, Budget, budget) {
		$scope.typed = "";
		$scope.budget = budget;
		$scope.loadingRemove = false;
		$scope.action = $translate.instant("account.delete.type");
		$scope.cancel = function () {
			$scope.loadingRemove = false;
			$modalInstance.dismiss('cancel');
		};
		$scope.removeBudget = function() {
			$scope.loadingRemove = true;
			var budget = Budget.remove({
				"uuid" : $scope.budget.uuid
			}, function() {
				var uuid = $scope.budget.uuid;
				$modalInstance.dismiss('cancel');
				Array.remove($rootScope.budgets, uuid);
				$rootScope.$broadcast("budget-changed");
				$state.go("budget");
				Alert.show($translate.instant("budget.delete.success"));
			}, function (err) {
				$modalInstance.dismiss('cancel');
				Alert.warn($translate.instant("budget.delete.error"));
			});
		};
	}]);

})(angular);