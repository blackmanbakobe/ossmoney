(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetEditController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "Budget", "Array", "Alert", function($rootScope, $scope, $state, $stateParams, $translate, Budget, Array, Alert) {
		$scope.loading = false;
		$scope.toValidation = "";
		$scope.fromValidation = "";
		$scope.nameValidation = "";
		$scope.balanceValidation = "";
		$scope.budget = $rootScope.budgets[$stateParams.budget - 1];

		$scope.openTo = function($event) {
			$scope.openedTo = true;
			$scope.openedFrom = false;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.openFrom = function($event) {
			$scope.openedTo = false;
			$scope.openedFrom = true;
			$event.preventDefault();
			$event.stopPropagation();
		};

		$scope.saveBudget = function() {
			$scope.toValidation = !$scope.budget.to ? "has-error" : "";
			$scope.fromValidation = !$scope.budget.from ? "has-error" : "";
			$scope.nameValidation = !$scope.budget.name ? "has-error" : "";
			$scope.balanceValidation = !$scope.budget.amount.number ? "has-error" : "";
			if (!$scope.nameValidation & !$scope.fromValidation && !$scope.toValidation & !$scope.balanceValidation) {
				$scope.loading = true;
				var dto = angular.copy($scope.budget);
				delete dto.spent;
				delete dto.transactions;
				dto.amount = dto.amount.number;
				dto.subcategory = dto.subcategory.tag;
				var budget = Budget.save(dto, function() {
					$scope.loading = false;
					if (budget && budget.uuid) {
						Array.remove($rootScope.budgets, budget.uuid);
						$rootScope.budgets.push(budget);
						$rootScope.$broadcast("budget-changed");
						$state.go("budget");
						Alert.show($translate.instant("budget.save.success"));
					} else {
						Alert.warn($translate.instant("budget.save.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("budget.save.error"));
				}); 
			}
		};
	}]);

})(angular);