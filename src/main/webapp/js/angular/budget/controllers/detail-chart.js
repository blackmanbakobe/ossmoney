(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetOverTimeController", ["$rootScope", "$scope", "$stateParams", "$translate", function($rootScope, $scope, $stateParams, $translate) {
		$scope.data_budget = [];
		if ($rootScope.skin === "brown") {
			$scope.colours_budget = [
				{fillColor: "rgba(194,100,72,0.2)", strokeColor: "rgba(194,100,72,1)", pointColor: "rgba(194,100,72,1)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(194,100,72,0.8)"},
				{fillColor: "rgba(217,178,113,0.2)", strokeColor: "rgba(217,178,113,1)", pointColor: "rgba(217,178,113,1)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(217,178,113,0.8)"}
			];
		}
		var currentMonth = new Date().getMonth();
		$scope.budget = $rootScope.budgets[$stateParams.budget - 1];
		$scope.series_budget = [$translate.instant("budget.actual"), $translate.instant("budget.estimation")];
		var monthData = [
			{"month": $translate.instant("month." + (currentMonth - 5 < 0 ? currentMonth + 7 : currentMonth - 5)), "spent": 0},
			{"month": $translate.instant("month." + (currentMonth - 4 < 0 ? currentMonth + 8 : currentMonth - 4)), "spent": 0},
			{"month": $translate.instant("month." + (currentMonth - 3 < 0 ? currentMonth + 9 : currentMonth - 3)), "spent": 0},
			{"month": $translate.instant("month." + (currentMonth - 2 < 0 ? currentMonth + 10 : currentMonth - 2)), "spent": 0},
			{"month": $translate.instant("month." + (currentMonth - 1 < 0 ? currentMonth + 11 : currentMonth - 1)), "spent": 0},
			{"month": $translate.instant("month." + currentMonth), "spent": 0}
		];
		var data = [], labels = [];
		for (var i  = 0; i < monthData.length; i++) {
			labels.push(monthData[i].month);
			data.push($scope.budget.amount.number);
		}
		$scope.labels_budget = labels;

		$scope.calculateBucket = function(date, currentMonth) {
			var month = currentMonth - Number(date.substring(5, 7)) + 1;
			return month < 0 ? month + 12 : month;
		};

		angular.forEach($scope.budget.transactions, function(transaction) {
			var bucket = monthData.length - $scope.calculateBucket(transaction.operationDate, currentMonth) - 1;
			if ((bucket > 0) & (bucket < monthData.length)) {
				monthData[bucket].spent += Math.abs(transaction.amount.number);
			}
		});
		var spent = [];
		angular.forEach(monthData, function(monthData) {
			spent.push(monthData.spent);
		});
		$scope.data_budget.push(spent);
		$scope.data_budget.push(data);
	}]);

})(angular);