(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetTransactionsController", ["$rootScope", "$scope", "$translate", "Budget", function($rootScope, $scope, $translate, Budget) {
		$scope.rowCollection = [];
		$scope.monthlyBudgetTransactions = [];
		$scope.recalculateBudgetTransactions = function() {
			var date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
			angular.forEach($rootScope.budgetTransactions, function(transaction) {
				if (new Date(transaction.operationDate).getTime() > firstDay) {
					$scope.monthlyBudgetTransactions.push(transaction);
				}
			});
		};
		$rootScope.$on("budget-changed", function() {
			$scope.recalculateBudgetTransactions();
		});
		$scope.recalculateBudgetTransactions();
	}]);

})(angular);