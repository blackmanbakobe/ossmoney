(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.config(["$stateProvider", function($stateProvider) {
		$stateProvider.state("add_budget", {
			"controller": "OssmoneyBudgetAddController",
			"templateUrl": "views/budget/add.view.html"
		}).state("view_budget", {
			"templateUrl": "views/budget/detail.view.html",
			"params": {
				"budget": null
			}
		});
	}]);

})(angular);