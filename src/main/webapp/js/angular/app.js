(function(window, document, angular) {

	var app = angular.module("ossmoney",
		["chart.js", "ngResource", "ngAnimate", "ui.router", "ui.bootstrap", "tmh.dynamicLocale", "pascalprecht.translate", "ossmoneyMenu", "ossmoneyAdmin", "ossmoneyAccount", "ossmoneyBudget", "ossmoneyDashboard", "ossmoneyInvestment"])
			.value("selection", "account");

	app.config(["$translateProvider", function($translateProvider) {
		$translateProvider
			.registerAvailableLanguageKeys(["es", "en"], {
				"en_US": "en",
				"en_UK": "en",
				"es_ES": "es"
			}).determinePreferredLanguage();
	}]);

	app.config(function (datepickerConfig) {
		datepickerConfig.showWeeks = false;
	});

	app.config(["$stateProvider", function($stateProvider) {
		$stateProvider
			.state("blank", {
				"params": {"option": {}},
				"template": "<div>&nbsp;</div>",
				"controller": function($state, $stateParams) {
					$state.go($stateParams.option);
				}
			}).state("dashboard", {
				"templateUrl": "views/dashboard/index.view.html"
			}).state("account", {
				"controller": "OssmoneyAccountInitController",
				"templateUrl": "views/account/index.view.html"
			}).state("investment", {
				"templateUrl": "views/investment/index.view.html"
			}).state("budget", {
				"controller": "OssmoneyBudgetInitController",
				"templateUrl": "views/budget/index.view.html"
			}).state("forecast", {
				"templateUrl": "views/forecast/index.view.html"
			});
	}]);

	app.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"OK": "Continuar",
			"CANCEL": "Cancelar",
			"EUR": "€",
			"USD": "$",
			"GBP": "£",
			"NO": "No",
			"YES": "Sí",
			"user.locale": "Idioma y moneda",
			"es_ES": "Español",
			"en_US": "Inglés",
			"user.skin": "Tema",
			"user.skin.brown": "Marrón",
			"user.skin.blue": "Azul",
			"user.currency": "Moneda",
			"preferences.title": "Preferencias",
			"save.preferences": "Guardar preferencias",
			"save.preferences.success": "Preferencias actualizadas",
			"save.preferences.error": "Error almacenando las preferencias",
			"country.es_ES": "España",
			"country.en_US": "EEUU",
			"country.en_UK": "Reino unido"
		});
		$translateProvider.translations('en', {
			"OK": "Ok",
			"CANCEL": "Cancel",
			"EUR": "€",
			"USD": "$",
			"GBP": "£",
			"NO": "No",
			"YES": "Yes",
			"user.locale": "Language & currency",
			"es_ES": "Spanish",
			"en_US": "English",
			"user.skin": "Theme",
			"user.skin.brown": "Brown",
			"user.skin.blue": "Blue",
			"user.currency": "Currency",
			"preferences.title": "Preferences",
			"save.preferences": "Save preferences",
			"save.preferences.success": "Preferences updated",
			"save.preferences.error": "Error saving preferences",
			"country.es_ES": "Spain",
			"country.en_US": "USA",
			"country.en_UK": "United Kingdom"
		});
	}]);

})(window, document, angular);