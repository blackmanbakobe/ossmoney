(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyTransactionRemoveController", ["$scope", "$modal", function($scope, $modal) {
		$scope.showModal = function () {
			var modalInstance = $modal.open({
				"animation": "true",
				"templateUrl": "transaction.remove.modal.html",
				"controller": 'OssmoneyTransactionRemoveModalController',
				"resolve": {
					"account": function() {
						return $scope.account;
					}, "transaction": function () {
						return $scope.transaction;
					}
		        }
		    });
		};
	}]);

	account.controller("OssmoneyTransactionRemoveModalController", ["$rootScope", "$scope", "$modalInstance", "$state", "$translate", "Alert", "Transaction", "account", "transaction", function($rootScope, $scope, $modalInstance, $state, $translate, Alert, Transaction, account, transaction) {
		$scope.account = account;
		$scope.transaction = transaction;
		$scope.amount = Math.abs($scope.transaction.amount.number);
		$scope.translationData = {
			"amount": $scope.amount
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
		$scope.remove = function() {
			var account = Transaction.remove({
				"account": $scope.transaction.uuid
			}, function() {
				$modalInstance.dismiss('cancel');
				if (account !== null) {
					$scope.account = account;
					var index = $rootScope.accounts.map(function(account) {
						return account.uuid;
					}).indexOf($scope.account.uuid);
					$rootScope.accounts.splice(index, 1);
					$rootScope.accounts.push(account);
					$rootScope.$broadcast("accounts-changed");
					delete $rootScope.incomeVsExpenses;
					$rootScope.$broadcast("interval-changed", {
						"months": $rootScope.incomeVsExpensesFromDate
					});
					$rootScope.transactions[$scope.account.uuid] = [];
					$state.go("account_details", {
						"account": $scope.account
					});
					Alert.show($translate.instant("transaction.delete.success"));
				} else {
					$modalInstance.dismiss('cancel');
					Alert.warn($translate.instant("transaction.delete.error"));
				}
			}, function (err) {
				$modalInstance.dismiss('cancel');
				Alert.warn($translate.instant("transaction.delete.error"));
			});
		};
	}]);

})(angular);