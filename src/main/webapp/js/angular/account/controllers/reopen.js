(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountReopenController", ["$rootScope", "$scope", "$state", "$translate", "Reopen", "Array", "Alert", function($rootScope, $scope, $state, $translate, Reopen, Array, Alert) {
		$scope.loading = false;
		$scope.reopenAccount = function() {
			$scope.loading = true;
			var account = Reopen.save({
				"uuid": $scope.account.uuid
			}, function() {
				$scope.loading = false;
				if (account && account.currentBalance) {
					Array.remove($rootScope.accounts, $scope.account.uuid);
					$rootScope.accounts.push(account);
					$state.go("account_main");
					$rootScope.$broadcast("accounts-changed");
					Alert.show($translate.instant("account.reopen.success"));
				} else {
					Alert.warn($translate.instant("account.reopen.error"));
				}
			}, function (err) {
				$scope.loading = false;
				Alert.warn($translate.instant("account.reopen.error"));
			});
		};
	}]);

})(angular);