(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.factory("SearchTable", [function() {
		return function(scope, element, ctrl, text) {
			element.bind('click', function (evt) {
				var input = document.getElementById("table-search");
				input.focus();
				input.scrollIntoView();
				input.value = text;
				ctrl.search(text, "");
				document.activeElement.blur();
				if (scope.$parent.$parent.clearReconciliation) {
					scope.$parent.$parent.clearReconciliation();
				}
			});
		};
	}]);

	account.directive("tagFilter", function (SearchTable) {
		return {
			"require": "^stTable",
			"scope": true,
			"link": function (scope, element, attr, ctrl) {
				SearchTable(scope, element, ctrl, scope.tag.text);
			}
		};
	});

	account.directive("textFilter", function (SearchTable) {
		return {
			"require": "^stTable",
			"scope": {
				"textFilter": "@"
			}, "link": function (scope, element, attr, ctrl) {
				SearchTable(scope, element, ctrl, scope.textFilter);
			}
		};
	});

	account.directive("searchClear", function (SearchTable) {
		return {
			"require": "^stTable",
			"scope": true,
			"link": function (scope, element, attr, ctrl) {
				SearchTable(scope, element, ctrl, "");
			}
		};
	});

	account.directive('csSelect', function () {
	    return {
	        require: '^stTable',
	        template: '<input type="checkbox"/>',
	        scope: {
	            row: '=csSelect'
	        },
	        link: function (scope, element, attr, ctrl) {
	            element.bind('change', function (evt) {
	            	scope.$parent.$parent.addMovementToReconcile(scope.$parent.$parent.transaction);
	                scope.$apply(function () {
	                    ctrl.select(scope.row, 'multiple');
	                });
	            });
	        }
	    };
	});

})(angular);