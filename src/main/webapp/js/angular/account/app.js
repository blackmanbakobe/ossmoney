(function(angular) {

	var account = angular.module("ossmoneyAccount", ["smart-table", "ngTagsInput", "ngResource", "ui.router", "pascalprecht.translate"]);

	account.run(["$rootScope", function($rootScope) {
		$rootScope.banks = {};
		$rootScope.transactions = {};
	}]);

	account.run(["$rootScope", "AccountType", "Tags", function($rootScope, AccountType, Tags) {
		$rootScope.tags = Tags.query();
		$rootScope.accountTypes = AccountType.query();
	}]);

	account.run(["$rootScope", function($rootScope) {
		$rootScope.lateralMenuOptions = [{
			"admin": false,
			"onclick": "none",
			"navigation" : "account_main",
			"option" : "menu_accounts_list"
		}, {
			"admin": false,
			"onclick": "none",
			"navigation" : "account_add",
			"option" : "menu_accounts_add"
		}, {
			"admin": false,
			"onclick": "none",
			"navigation" : "deposit_add",
			"option" : "menu_deposit_add"
		}, {
			"admin": false,
			"onclick": "menu_pm",
			"navigation" : "pm_list",
			"option": "menu_pm",
			"options": [
				{
					"navigation" : "pm_list",
					"option": "menu_pm_list"
				}, {
					"navigation" : "pm_buy",
					"option": "menu_pm_buy"
				}, {
					"navigation" : "pm_prices",
					"option": "menu_pm_prices"
				}
			]
		}, {
			"admin": false,
			"onclick": "none",
			"navigation" : "preferences",
			"option" : "menu_preferences"
		}, {
			"admin": true,
			"navigation" : "entities",
			"option": "menu_administration",
			"onclick": "menu_administration",
			"options": [
				{
					"navigation" : "entities",
					"option": "menu_entities"
				}, {
					"navigation" : "metals",
					"option": "menu_metals"
				}
			]
		}];
		$rootScope.lateralMenu = $rootScope.lateralMenuOptions[0];
	}]);

	account.run(["$rootScope", "Account", function($rootScope, Account) {
		var accounts = Account.query(function() {
			$rootScope.accounts = accounts;
			angular.forEach($rootScope.accounts, function (account) {
				$rootScope.transactions[account.uuid] = [];
			});
			$rootScope.$broadcast("accounts-changed");
		});
	}]);

})(angular);