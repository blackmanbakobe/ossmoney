(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardTotalsController", ["$rootScope", "$scope", function($rootScope, $scope) {
		$scope.totalCredit = 0;
		$scope.totalIncome = 0;
		$scope.totalExpenses = 0;
		$scope.recalculateTotals = function() {
			angular.forEach($rootScope.accounts, function(account) {
				if (account.creditCard) {
					$scope.totalCredit -= Math.abs(account.currentBalance.number - account.initialBalance.number);
				}
			});
			$scope.totalIncome = $rootScope.timeline.reduce(function(previousValue, income) {
				return previousValue + income["INCOME"];
			}, 0);
			$scope.totalExpenses = $rootScope.timeline.reduce(function(previousValue, expenses) {
				return previousValue + expenses["EXPENSE"];
			}, 0);
		};
		$rootScope.$on("expenses-changed", function() {
			$scope.recalculateTotals();
		});
	}]);

})(angular);