(function(angular) {

	var admin = angular.module("ossmoneyAdmin");

	admin.controller("OssmoneyAdminMetalsController", ["$rootScope", "$scope", "$state", "$translate", "Metal", "Alert", function($rootScope, $scope, $state, $translate, Metal, Alert) {
		$scope.loading = false;
		$scope.metal = {
			"url": "",
			"name": "",
			"shape": "COIN",
			"metalType": "GOLD",
			"mint": "UNKNOWN_MINT"
		};
		$scope.saveMetal = function() {
			$scope.nameValidation = !$scope.metal.name ? "has-error has-feedback" : "";
			if (!$scope.nameValidation) {
				$scope.loading = true;
				var product = Metal.save($scope.metal, function() {
					$scope.loading = false;
					if (product && product.uuid) {
						if ($rootScope.preciousMetalProducts !== undefined) {
							$rootScope.preciousMetalProducts.push(product);
							$rootScope.$broadcast("precious-metals-products-changed");
						}
						$state.go("account_main");
						Alert.show($translate.instant("pm.add.create.success"));
					} else {
						Alert.warn($translate.instant("pm.add.create.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("pm.add.create.error"));
				});
			}
		};
	}]);

})(angular);