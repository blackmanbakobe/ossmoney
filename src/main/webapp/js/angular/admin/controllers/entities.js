(function(angular) {

	var admin = angular.module("ossmoneyAdmin");

	admin.controller("OssmoneyAdminEntitiesController", ["$rootScope", "$scope", "$state", "$translate", "Entity", "Alert", function($rootScope, $scope, $state, $translate, Entity, Alert) {
		$scope.loading = false;
		$scope.entity = {
			"name": "",
			"country": {
				"countryKey": "country." + $rootScope.locale
			}
		};
		$scope.saveEntity = function() {
			$scope.nameValidation = !$scope.entity.name ? "has-error has-feedback" : "";
			if (!$scope.nameValidation) {
				$scope.loading = true;
				var entity = Entity.save($scope.entity, function() {
					$scope.loading = false;
					if ($rootScope.banks[$scope.entity.country.countryKey]) {
						$rootScope.banks[$scope.entity.country.countryKey].push(entity);
					}
					$state.go("account_main");
					Alert.show($translate.instant("entity.create.success"));
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("entity.create.error"));
				});
			}
		};
	}]);

})(angular);