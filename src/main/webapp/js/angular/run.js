(function(angular) {

	var app = angular.module("ossmoney");

	app.run(["$rootScope", "tmhDynamicLocale", "$translate", "User", "ChartColor", function($rootScope, tmhDynamicLocale, $translate, User, ChartColor) {
		$rootScope.skin = "none";
		$rootScope.loading = false;
		var user = User.rest.query(function() {
			User.configure($rootScope, $translate, tmhDynamicLocale, ChartColor, user);
		})
	}]);

	app.run(["$rootScope", function($rootScope) {
		$rootScope.alert = {
			"hide": true,
			"message": "",
			"type": "success",
			"close": function() {
				this.hide = true;
			}, "top": Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 75
		};
	}]);

})(angular);